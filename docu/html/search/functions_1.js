var searchData=
[
  ['camera',['Camera',['../class_camera.html#a01f94c3543f56ede7af49dc778f19331',1,'Camera::Camera()'],['../class_camera.html#ada8e79271c7ad3646f7445a385e48750',1,'Camera::Camera(string id, string dist_model, Mat dist_coeffs, Mat camera_matrix, unsigned int height, unsigned int width)']]],
  ['camerainfoleftcallback',['CameraInfoLeftCallback',['../class_pallet_detection.html#aa6192463651b23e2ab953422f451816c',1,'PalletDetection']]],
  ['camerainforightcallback',['CameraInfoRightCallback',['../class_pallet_detection.html#a5ad29a3c9436db78164ae18cf88469ef',1,'PalletDetection']]],
  ['convertpointsfromhomogeneous',['convertPointsFromHomogeneous',['../class_mat_utils.html#a59917fd48bea27bbbfdc408ea26f7176',1,'MatUtils']]],
  ['convertpointstohomogeneous',['convertPointsToHomogeneous',['../class_mat_utils.html#acc5299ea359bc2ea1a6ad993f079ad70',1,'MatUtils']]]
];
