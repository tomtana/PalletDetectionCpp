//
// Created by tman on 3/1/17.
//

#ifndef PALLET_DETECTION_CVMORPHOLOGY_H
#define PALLET_DETECTION_CVMORPHOLOGY_H

#include <opencv2/core/core.hpp>

class CVMorphology {
    public:
    CVMorphology();
    static cv::Mat getLine(double angle, double length);
    static cv::Mat reconstructByDilation(const cv::Mat &marker_img,const cv::Mat &mask_img, unsigned int iter=0 ,bool full_connectivity=false);
};


#endif //PALLET_DETECTION_CVMORPHOLOGY_H
