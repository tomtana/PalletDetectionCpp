//
// Created by tman on 3/2/17.
//

#ifndef PALLET_DETECTION_FASTDCM_H
#define PALLET_DETECTION_FASTDCM_H


#include <LMLineMatcher.h>
#include <string.h>
#include <opencv2/core/core.hpp>
#include <stdlib.h>

struct LfSettings{
    // Load configuration
public:
    double sigma_fit_a_line;
    double sigma_find_support;
    double max_gap;
    double nLinesToFitInStage1;
    double nTrialsPerLineInStage1;
    double nLinesToFitInStage2;
    double nTrialsPerLineInStage2;
    int nLayer;
    int nLinesToFitInStage[2];
    int nTrialsPerLineInStage[2];
    /**
     * Init struct with hardcoded values
     */
    LfSettings(){
        sigma_fit_a_line=0.5;
        sigma_find_support=0.5;
        max_gap=2.0;
        nLinesToFitInStage1=0;
        nTrialsPerLineInStage1=0;
        nLinesToFitInStage2=10000;
        nTrialsPerLineInStage2=1;
        nLayer = 2;
        nLinesToFitInStage[0] = (int)floor(nLinesToFitInStage1);
        nLinesToFitInStage[1] = (int)floor(nLinesToFitInStage2);
        nTrialsPerLineInStage[0] = (int)floor(nTrialsPerLineInStage1);
        nTrialsPerLineInStage[1] = (int)floor(nTrialsPerLineInStage2);
    }
    void dump(){
        cout<<"LfSettings:"<<endl;
        cout<<"sigma_fit_a_line: "<<sigma_fit_a_line<<endl;

        cout<<"max_gap: "<<max_gap<<endl;
        cout<<"nLinesToFitInStage1: "<<nLinesToFitInStage1<<endl;
        cout<<"nTrialsPerLineInStage1: "<<nTrialsPerLineInStage1<<endl;
        cout<<"nLinesToFitInStage2: "<<nLinesToFitInStage2<<endl;
        cout<<"nLayer: "<<nLayer<<endl;
        cout<<"nLinesToFitInStage[0]: "<<nLinesToFitInStage[0]<<endl;
        cout<<"nLinesToFitInStage[1]: "<<nLinesToFitInStage[1]<<endl;
        cout<<"nTrialsPerLineInStage[0]: "<<nTrialsPerLineInStage[0]<<endl;
        cout<<"nTrialsPerLineInStage[1]: "<<nTrialsPerLineInStage[1]<<endl;
        cout<<"----------END-----------\n"<<endl;
    }
    /**
     * Add constructor to load values from file
     */
};

class FastDCM {


private:
    LMLineMatcher lm;
    LfSettings lf_conf;
public:
    /**
     * Loads the settings from a yaml file and inits the line matcher
     * @param path
     */
    FastDCM(string path);
    /**
     * Inits the FDCM with hardcoded values
     */
    FastDCM();
    /**
     * Performs the detection
     */
    bool performDetection(cv::Mat img, const std::vector<cv::Mat > & templates, double confidence,
                         vector<vector<LMDetWind> > & result);

    /**
     * Performs the line fit to bring a mat in the adequate format to apply to the FDCM
     */
     void performLineFit(cv::Mat img);
};


#endif //PALLET_DETECTION_FASTDCM_H
