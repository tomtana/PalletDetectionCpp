//
// Created by tman on 2/21/17.
//

#ifndef PALLET_DETECTION_ITKMORPHOLOGY_H
#define PALLET_DETECTION_ITKMORPHOLOGY_H

#include <opencv2/core/core.hpp>
#include <itkFlatStructuringElement.h>
#include <itkImage.hxx>

typedef itk::Image<unsigned char, 2>    ItkUint8Img;
typedef itk::Image<bool, 2>    ItkBinImg;
typedef itk::Image<float, 2>    ItkFloat32Img;
typedef itk::Image<double, 2>    ItkDouble64Img;
typedef itk::Image<int, 2>    ItkInt32Img;
typedef itk::Image<unsigned int, 2>    ItkUint32Img;



class ITKMorphology {
public:
    ITKMorphology();

    /**
     * Converts from OpenCv to ITK
     * only considers 2 Dim
     * @tparam T
     * @param cv_img
     * @param itk_img
     */
    template <typename TImage> static typename TImage::Pointer CVtoITK(cv::Mat cv_img);
    template<typename TImage> static cv::Mat ITKtoCV(typename TImage::Pointer itk_img);
    template<typename TImage> static typename TImage::Pointer computeErosion(typename TImage::Pointer image_in, itk::FlatStructuringElement< 2 > kernel);
    /**
     * Computes the erosion with line elements given in a std vector. The output is a bin Or result of all the erosions
     * @tparam TImage
     * @param image_in
     * @param param_line line parameters with param_line[i][0]= length  and param_line[i][1]= angle in degree
     * @return
     */
    template<typename TImage> static typename TImage::Pointer computeErosionByLines(typename TImage::Pointer image_in, std::vector< std::vector<double> > param_line);
    template<typename TImage> static typename TImage::Pointer computeReconstructionByDilation(typename TImage::Pointer image_to_reconstr, typename TImage::Pointer image_to_compare, bool fully_connected=false);

    static itk::FlatStructuringElement< 2 > getLine(double length, double angle);

    template<typename TImage> static typename TImage::Pointer Or(typename TImage::Pointer image1,typename TImage::Pointer image2);

    static void openByReconstructionWithLines(const cv::Mat &img_in, cv::Mat &img_out,
                                              std::vector<std::vector<double> > param_line,
                                              bool fully_connected = false);

};


#endif //PALLET_DETECTION_ITKMORPHOLOGY_H
