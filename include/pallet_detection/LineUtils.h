//
// Created by tman on 12/6/16.
//

#ifndef PALLET_DETECTION_LINEUTILS_H
#define PALLET_DETECTION_LINEUTILS_H


#include "PPoint.h"
#include <opencv2/core/core.hpp>

namespace LineUtils {
    cv::Point2f    getIntersection(cv::Vec4f L1,cv::Vec4f L2);
    std::vector<std::vector<cv::Point2f> > getIntersection(std::vector<cv::Vec4f> L1,std::vector<cv::Vec4f> L2);
    cv::Vec4f   getCartesian(cv::Vec2f line,cv::Rect border);
    cv::Vec4f   getCartesian(cv::Vec2f line,int x_min,int x_max, int y_min, int y_max);
    std::vector<cv::Vec4f>   getCartesian(std::vector<cv::Vec2f> lines,cv::Rect border);
    std::vector<cv::Vec4f>   getCartesian(std::vector<cv::Vec2f> lines,int x_min,int x_max, int y_min, int y_max);
    float getX(cv::Vec4f line, int y);
    float getX(cv::Vec2f line, int y);
    float getY(cv::Vec4f line, int x);
    float getY(cv::Vec2f line, int x);
    bool isInsideRoi(cv::Point2i point, cv::Rect roi);
    /**
     * Filters line vector by angle
     *
     * This function filters a given line vector, which is defined by 2 Points, by an angle. All angles within the range
     * are passed back
     * @param angle the angle
     * @param angle_derivation the acceptable derivation from that angle
     * @return the filtered lines respecting the angle limits
     */
    std::vector<cv::Vec4f> filterLinesByAngle(std::vector<cv::Vec4f> lines,float angle, float angle_derivation);
    std::vector<cv::Vec4f> filterLinesByAngleDeg(std::vector<cv::Vec4f> lines,float angle, float angle_derivation);

    double computeAngle(cv::Vec4f line);
    cv::Vec4f getIntersectionWithRoi(cv::Vec4f line,cv::Rect roi);
    cv::Vec4f getIntersectionWithRoi(cv::Vec4f line,int x_min,int x_max, int y_min, int y_max);
    std::vector<cv::Vec4f> getIntersectionWithRoi(std::vector<cv::Vec4f> lines,cv::Rect roi);
    std::vector<cv::Vec4f> getIntersectionWithRoi(std::vector<cv::Vec4f> lines,int x_min,int x_max, int y_min, int y_max);
    //get a rectangular area which is defined by lines. It extracts the maximum values and defines the Roi
    cv::Rect BoundingRect(std::vector<cv::Vec4f> lines);
    cv::Vec4f RectToVec(cv::Rect rect);
    float getHorizDist(cv::Vec4f L1,cv::Vec4f L2, float x);
    std::vector<float> getHorizDist(std::vector<cv::Vec4f> L1,std::vector<cv::Vec4f> L2, float x);
    float getVerticDist(cv::Vec4f L1,cv::Vec4f L2, float y);

    //include maybe also an angle derivation parameter to limit the angles two horizonzal lines can diverge
    std::vector<std::vector<cv::Vec4f> > filterHorizDist(std::vector<cv::Vec4f> lines, float dist, float dist_derivation,float x, float angle_diff,int max_x_dist);

    bool isInList(std::vector<int> list, int i);
    //template to convert vector Vec types between different datatypes... float, double int..
    template <typename T_target, typename T_origin, int n> std::vector<cv::Vec<T_target,n> > convertVecType (std::vector<cv::Vec<T_origin,n> > input);

    //function to test if the angle difference between 2 functions is smaller that a given value
    bool areInAngleRange(cv::Vec4f L1,cv::Vec4f L2, float angle_diff);

    //template <typename T,int n> void sortVec(std::vector<cv::Vec<T,n> >& vec);
};


#endif //PALLET_DETECTION_LINEUTILS_H
