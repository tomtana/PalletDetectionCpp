//
// Created by tman on 17/11/16.
//

#ifndef PALLET_DETECTION_MATUTILS_H
#define PALLET_DETECTION_MATUTILS_H

//Cv includes
#include <opencv2/core/core.hpp>
//stdlib includes
#include <stdlib.h>
#include <iostream>
//Project includes
#include "PPoints.h"
#include "PPoints3.h"
//Ros includes
#include <image_geometry/pinhole_camera_model.h>
#include <tf/transform_datatypes.h>


/**
 * \brief helper functions
 *
 * The class contains static helper functions to manipulate and convert mat objects
 */
namespace MatUtils {

    /**
     * \brief Converts points to a homogeneous points
     *
     * Adds to a n x m matrix, containing m points of the dimension n, a row with 1s
     * @param input the matrix which is to be transformed
     * @param output the transformed matrix
     */
    void convertPointsToHomogeneous(cv::Mat input,cv::Mat & output);


    /**
     * \brief Converts homogeneous points back
     *
     * Removes from a n x m matrix, containing m homogeneous points of the dimension n, the homogeneous component by diviging
     * each point by its nth compoment.
     * @param input the matrix which is to be transformed
     * @param output the transformed matrix
     */
    void convertPointsFromHomogeneous(cv::Mat input, cv::Mat& output);

    /**
     * \brief Computes distances bewteen points
     *
     * Computes all the distances between points and ref_points. For a mxn point matrix and a mxb ref_point matrix,
     * the resulting distance matrix has the dimension bxn
     *
     *
     * @param points point matrix with dimension mxn
     * @param ref_points reference point matrix with dimension mxb
     * @param distances resulting distances with dimension bxn
     */
    void computePointDistances(const cv::Mat points, const cv::Mat ref_points, cv::Mat& distances);

    /**
     * \brief Converts Mat to Point
     *
     * This function converts Mat objects to the right point object. However, if Mat contains several points,
     * a vector object has to be passed instead of a simple point. Also make sure that you use the right point object (point2d,etc)
     *
     * @param m_point mxn input Matrix containing n points of dimension m
     * @param point the converted point object. Make sure you pass a std::vector in case n>1 of the input Matrix
    */
    template <typename T> PPoints3<T> MatToPPoint3(const cv::Mat& m_point);

    template <typename T> PPoints<T> MatToPPoint2(const cv::Mat& m_point);

    /**
     * \brief Transforms a cv::Mat in a homogeneous mat by adding a row with zeros and as last element of the row a 1
     * @param trans_mat the transformation matrix without the homogeneous element of type double or float
     * @param output homogeneous transformation matrix cv::Mat of type double or float depending on the input Mat type
     */
    void convertMatToHomogeneousMat(const cv::Mat trans_mat, cv::Mat& output);

    std::vector<PPoints2d> projectPoints3ToImage(std::vector<PPoints3d> points, const image_geometry::PinholeCameraModel& cam_model);

    /**
     * \brief Converts ros Matrix3x3 to a 4x4 cv:Mat transformation matrix
     *
     * in fact the ros matrix is not a 3x3 but a 3x4 matrix given back by the getOpenGLSubMatrix() function as a column major array.
     * Where the last element of each row is by default 0.
     * Check out the .h file of the declatarion http://docs.ros.org/api/tf/html/c++/Matrix3x3_8h_source.html#l00135
     * and the ros class docu http://docs.ros.org/api/tf/html/c++/classtf_1_1Matrix3x3.html
     * @param ros_mat 3x4 ros input matrix represented by a 12 element array of type tfScalar which is a typedef of double
     * @return A 4x4 homogeneous transformation Matrix cv:Mat of double precision
    */
    cv::Mat RosMat3x3toHomCvMat(tf::Matrix3x3 ros_mat);
    /**
     * \brief Converts ros Vector3 to a 4x4 cv:Mat transformation matrix
     *
     * The Vector3 is interpreted as translational part of a transformation matrix.
     * In fact the ros vector is not a 3x1 but a 4x1 vector. the last element is 0.
     * check out the .h file of the declatarion http://docs.ros.org/api/tf/html/c++/Vector3_8h_source.html
     * and the ros class docu http://docs.ros.org/api/tf/html/c++/classtf_1_1Vector3.html
     * @param ros_vec 4x1 ros input vector represented by a 4 element array of type tfScalar which is a typedef of double
     * @return A 4x4 cv:Mat transformation matrixof double precision with idendity as rotational part
    */
    cv::Mat RosVector3toHomCvMat(tf::Vector3 ros_vec);

    /**
     * \brief converts ros tf::Transform to a cv::Mat 4x4 homogeneous transformation matrix
     * @param ros_tf the ros class which holds the time stamp and the transform
     * @return A 4x4 cv:Mat of type double which represents the homogeneous transformation matrix
     */
    cv::Mat RosTransformtoHomCvMat(tf::Transform ros_tf);

};




#endif //PALLET_DETECTION_MATUTILS_H
