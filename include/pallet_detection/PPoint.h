//
// Created by tman on 23/11/16.
//

#ifndef PALLET_DETECTION_PALLETPOINTS_H
#define PALLET_DETECTION_PALLETPOINTS_H
#include <opencv2/core/core.hpp>
#include <stdlib.h>
#include <typeinfo>
#include <iostream>
//#include "MatUtils.h"





template <typename T>class PPoint: public cv::Point_<T> {
public:
    PPoint();
    PPoint(T x, T y);
    PPoint(cv::Point_<T> point);
    cv::Mat toMat();
    operator cv::Point_<T>();


};

//typedefs for most common types
typedef PPoint<int> PPoint2i;
typedef PPoint<float> PPoint2f;
typedef PPoint<double> PPoint2d;

template <typename T> PPoint<T> operator * (const cv::Mat& m, PPoint<T> point);

#endif //PALLET_DETECTION_PALLETPOINTS_H