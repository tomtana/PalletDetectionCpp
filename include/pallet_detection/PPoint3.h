//
// Created by tman on 24/11/16.
//

#ifndef PALLET_DETECTION_PPOINT3_H
#define PALLET_DETECTION_PPOINT3_H


#include <opencv2/core/core.hpp>
#include <stdlib.h>
#include <typeinfo>
#include <iostream>



template <typename T>class PPoint3: public cv::Point3_<T> {
public:
    PPoint3();
    PPoint3(T x, T y, T z);
    PPoint3(cv::Point3_<T> point);
    cv::Mat toMat();
    operator cv::Point3_<T>();
};


template <typename T> PPoint3<T> operator * (const cv::Mat& m, PPoint3<T> point);


//typedefs for most common types
typedef PPoint3<int> PPoint3i;
typedef PPoint3<float> PPoint3f;
typedef PPoint3<double> PPoint3d;



#endif //PALLET_DETECTION_PPOINT3_H