//
// Created by tman on 24/11/16.
//

#ifndef PALLET_DETECTION_PPOINTS_H
#define PALLET_DETECTION_PPOINTS_H

#include <stdlib.h>
#include "PPoint.h"


template <typename T>class PPoints {
private:
    std::vector< PPoint<T> > points_;

public:
    PPoints();
    PPoints(std::vector<PPoint<T> > points);
    PPoints(std::vector<cv::Point_<T> > points);
    //PPoints(PPoints & copy);
    cv::Mat toMat();
    std::vector<PPoint<T> >& getPoints();
    PPoint<T> &operator [](int idx);
    //std::vector< PPoint<T> > &operator =(std::vector< PPoint<T> > points);
    PPoints<T>& operator =(std::vector< cv::Point_<T> > points);
    PPoints<T>& operator =(std::vector< PPoint<T> > points);
    void push_back(cv::Point_<T> point);
    void push_back(PPoint<T> point);
    operator std::vector<cv::Point_<T> >();
};

template <typename T> PPoints<T> operator * (const cv::Mat& m, PPoints<T> points);


//typedefs for most common types
typedef PPoints<int> PPoints2i;
typedef PPoints<float> PPoints2f;
typedef PPoints<double> PPoints2d;
typedef std::vector<cv::Point2i> Points2i;
typedef std::vector<cv::Point2f> Points2f;
typedef std::vector<cv::Point2d> Points2d;


#endif //PALLET_DETECTION_PPOINTS_H