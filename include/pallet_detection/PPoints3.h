//
// Created by tman on 24/11/16.
//

#ifndef PALLET_DETECTION_PPOINTS3_H
#define PALLET_DETECTION_PPOINTS3_H


#include <stdlib.h>
#include <vector>
#include "PPoint3.h"




template <typename T> class PPoints3 {

public:
    std::vector< PPoint3<T> > points_;
    PPoints3();
    PPoints3(std::vector<PPoint3<T> > points);
    PPoints3(std::vector<cv::Point3_<T> > points);
    cv::Mat toMat();
    void push_back(PPoint3<T> point);
    operator std::vector<cv::Point3_<T> >();
    PPoint3<T>& operator [] (int idx);

};

template <typename T> PPoints3<T> operator * (const cv::Mat& m, PPoints3<T> points);

//typedefs for most common types
typedef PPoints3<int> PPoints3i;
typedef PPoints3<float> PPoints3f;
typedef PPoints3<double> PPoints3d;
typedef std::vector<cv::Point3i> Points3i;
typedef std::vector<cv::Point3f> Points3f;
typedef std::vector<cv::Point3d> Points3d;


#endif //PALLET_DETECTION_PPOINTS3_H