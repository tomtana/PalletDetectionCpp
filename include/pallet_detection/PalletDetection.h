//
// Created by tman on 26/10/16.
//

#ifndef PALLET_DETECTION_PALLETDETECTION_H
#define PALLET_DETECTION_PALLETDETECTION_H

//openCV includes
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/calib3d/calib3d.hpp"
//PCL includes
#include <pcl/visualization/pcl_visualizer.h>
//ROS includes
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <cv_bridge/cv_bridge.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/PoseArray.h>
#include <image_geometry/pinhole_camera_model.h>
#include <ros/package.h>
//std c/c++ includes
#include <string>
#include <stdlib.h>
//project includes
#include "PalletModel.h"
#include "MatUtils.h"
#include "UnitTest.h"
#include "PalletImage.h"
#include "PalletMarker.h"


using namespace cv;
using namespace std;
//to be changes to ROS PARAMETERS LATER
#define ROS_TOPIC_IMAGE_RECT_LEFT "/left/image_rect"
#define ROS_TOPIC_IMAGE_RECT_RIGHT "/right/image_rect"
#define ROS_TOPIC_IMAGE_LEFT "/left/image_raw"
#define ROS_TOPIC_CAM_INFO_LEFT "/left/camera_info"
#define ROS_TOPIC_IMAGE_RIGHT "/right/image_raw"
#define ROS_TOPIC_CAM_INFO_RIGHT "/right/camera_info"
/**
 * \brief This class performs the pallet detection
 *
 */


class PalletDetection{
private:
    //_____________ROS Parameters______________
    //Node handle
    ros::NodeHandle nh_; //!< The ros node handle
    //Topics
    string ros_topic_img_left_; //!< The topic name of the left input image
    string ros_topic_cam_info_left_; //!< The topic name of the left camera calibration
    string ros_topic_img_right_; //!< The topic name of the right input image
    string ros_topic_cam_info_right_; //!< The topic name of the right camera calibration
    //Initial callback check
    bool ros_recv_cam_info_left_; //!< Indicates if the calibation of the left camera was received
    bool ros_recv_cam_info_right_; //!< Indicates if the calibation of the right camera was received
    bool ros_recv_img_left_; //!< Indicates if the image of the left camera was received
    bool ros_recv_img_right_; //!< Indicates if the image of the right camera was received
    bool ros_recv_marker_pose; //!< Indicates if the marker pose was received
    bool ros_recv_pallet_model_to_cam; //!< Indicates if the transform from the pallet model to the cam was received
    //Image transport
    image_transport::ImageTransport ros_it_; //!< Ros image transport subscriber for receiving compressed input images
    image_transport::Subscriber ros_it_sub_left_; //!< Ros image transport subscriber to subscribe to left raw input image
    image_transport::Subscriber ros_it_sub_right_; //!< Ros image transport subscriber to subscribe to right raw input image
    //Subscribers
    ros::Subscriber ros_sub_cam_info_left_; //!< Ros subscriber for topic of left camera calibration
    ros::Subscriber ros_sub_cam_info_right_; //!< Ros subscriber for topic of right camera calibration
    //Publishers
    image_transport::Publisher ros_pub_img_undist_left_; //!< Ros publisher for left undistorted image
    image_transport::Publisher ros_pub_img_undist_right_; //!< Ros publisher for right undistorted image
    //Camera calibration parameters
    image_geometry::PinholeCameraModel ros_cam_model_left_; //!< Stores the camera calibrartion of the left camera
    image_geometry::PinholeCameraModel ros_cam_model_right_; //!< Stores the camera calibrartion of the right camera
    tf::TransformListener ros_mp_listener;
    //_____________Pallet Detection______________
    PalletModel pallet_model;
    bool pallet_detection_init;
    //Distorted Input Images
    PalletImage image_raw_left_; //!< The left raw input image
    PalletImage image_raw_right_; //!< The right raw input image
    //Undistorted Input Images
    PalletImage image_undist_left_; //!< The left undistorted input image
    PalletImage image_undist_right_; //!< The right undistorted input image
    //Marker pose
    PalletMarker marker_pose;
    cv::Mat cv_tf_pallet_model_to_cam_left;
    //pcl
    boost::shared_ptr<pcl::visualization::PCLVisualizer> pcl_viewer; //!< PCL visualizer

public:
    //Constructors
    PalletDetection(ros::NodeHandle & nh);

    //_____________ROS callback functions__________________
    /**
     * \brief Callback for left input image
     *
     * The callback function receives the raw input image and stores it. If the calibration was already received,
     * it further more computed and stores the undistorted image
     * @param img the raw input image
     */
    void ImageLeftCallback(const sensor_msgs::ImageConstPtr& img);
    /**
     * \brief Callback for right input image
     *
     * The callback function receives the raw input image and stores it. If the calibration was already received,
     * it further more computed and stores the undistorted image
     * @param img
     */
    void ImageRightCallback(const sensor_msgs::ImageConstPtr& img);
    /**
     * \brief Callback for left camera info
     *
     * The callback function receives and stores the camera calibration data of the left camera
     * @param cam_info the camera calibration data
     */
    void CameraInfoLeftCallback(const sensor_msgs::CameraInfoConstPtr & cam_info);
    /**
     * \brief Callback for right camera info
     *
     * The callback function receives and stores the camera calibration data of the right camera
     * @param cam_info the camera calibration data
     */
    void CameraInfoRightCallback(const sensor_msgs::CameraInfoConstPtr & cam_info);

    void GetTfPalletModel2Cam();
    /**
     * \brief Checks if a marker is detected and updates the marker position
     *
     * @return true if success, false if failed
     */
    bool GetMarkerPosition();
    /**
     * \brief Main function of the processing
     *
     * This function brings all subroutines together to exectue the pallet detection
     */
    void MainLoop();


    void init();


};


#endif //PALLET_DETECTION_PALLETDETECTION_H
