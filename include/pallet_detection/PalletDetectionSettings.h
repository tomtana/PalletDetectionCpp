#include <image_geometry/pinhole_camera_model.h>
//
// Created by tman on 16/11/16.
//

#ifndef PALLET_DETECTION_PALLETDETECTIONSETTINGS_H
#define PALLET_DETECTION_PALLETDETECTIONSETTINGS_H

#include <opencv2/core/core.hpp>
#include <image_geometry/pinhole_camera_model.h>


class PalletDetectionSettings {
public:
    PalletDetectionSettings();
    //for now this functions returns the Roi of the pallet according to the marker position.
    //in a further state this should be computed accourding to the pallet model loaded. For now it is hardcoded in the
    //function.
    static cv::Rect getPalletRoi(image_geometry::PinholeCameraModel camera_model, cv::Mat marker_pose);
};


#endif //PALLET_DETECTION_PALLETDETECTIONSETTINGS_H
