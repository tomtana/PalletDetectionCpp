//
// Created by tman on 1/21/17.
//

#ifndef PALLET_DETECTION_PALLETIMAGE_H
#define PALLET_DETECTION_PALLETIMAGE_H

#include <opencv2/core/core.hpp>
#include <std_msgs/Header.h>
#include <sensor_msgs/Image.h>


class PalletImage {
private:
    cv::Mat image_;
    std_msgs::Header header_;
public:
    PalletImage();
    PalletImage(cv::Mat image, std_msgs::Header header);
    std_msgs::Header getHeader();
    cv::Mat & getCvImage();
    cv::Mat getCvImageRoi(cv::Rect roi);
    void update(cv::Mat image, std_msgs::Header header);
    PalletImage &operator = (PalletImage & image);
};


#endif //PALLET_DETECTION_PALLETIMAGE_H
