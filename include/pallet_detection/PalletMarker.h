//
// Created by tman on 1/21/17.
//

#ifndef PALLET_DETECTION_PALLERMARKER_H
#define PALLET_DETECTION_PALLERMARKER_H

#include <tf/transform_datatypes.h>
#include <opencv2/core/core.hpp>
#include <image_geometry/pinhole_camera_model.h>

class PalletMarker {
private:
    cv::Mat marker_pose_;
    ros::Time stamp_;
public:
    PalletMarker();
    PalletMarker(tf::StampedTransform tf);
    PalletMarker(cv::Mat marker_pose);
    void update(tf::StampedTransform tf);
    //tf::StampedTransform getRosTransform();
    cv::Mat& getCvMatMarkerPose();
    ros::Time getRosTime();
    cv::Point3d getTranslation();
    cv::Point2i getImagePoint(image_geometry::PinholeCameraModel cam_model);
};


#endif //PALLET_DETECTION_PALLERMARKER_H
