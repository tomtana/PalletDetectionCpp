//
// Created by tman on 29/11/16.
//

#ifndef PALLET_DETECTION_PALLETMODEL_H
#define PALLET_DETECTION_PALLETMODEL_H

#include "PointModel.h"
#include "PPoints3.h"
#include <image_geometry/pinhole_camera_model.h>
#include <opencv2/core/core.hpp>
#include <ros/ros.h>


class PalletModel: public PointModel {
private:
    image_geometry::PinholeCameraModel cam_model_;
    std::vector<PPoints3<double> > points3_;
    //place the 3D points the the camera center
    void centerModel3();

public:

    PalletModel();
    PalletModel(const image_geometry::PinholeCameraModel& ros_cam_model_left_);
    bool load(std::string path);
    /**
     * Computes the distances between all the points in one line of the 2d model
     * @return
     */
    bool computeDistances();
    /**
     * \brief updates the point model
     *
     * Recomputes the current point model and the distances according to the transformation attribute.
     */
    void update(cv::Mat tf_pallet_model_2_cam, cv::Mat tf_cam_to_marker);

    void setCameraModel(image_geometry::PinholeCameraModel& cam_model);
    /**
     * Takes the 3D points defining the pallet and transforms them according to the homogeneous transformation attribute
     * @return A vector of points transformed to the pallet frame
     */
    std::vector<PPoints3d> transform3DCamToPallet(cv::Mat tf_pallet_model_2_cam, cv::Mat tf_cam_to_marker);
    /**
     * Returns the 3D points of the model at origin frame as cv:Mat
     * @return
     */
    cv::Mat getMatPoints3();

    /**
     * \brief returns a Mat with the drawn image
     *
     * this function is to be considered experimental and has to be integrated at a later stage with
     * more clean class bindings
     *
     * @return template of the pallet
     */
    cv::Mat draw(int thickness, int x_off, int y_off, double scale=1);


};


#endif //PALLET_DETECTION_PALLETMODEL_H
