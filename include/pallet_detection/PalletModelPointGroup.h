//
// Created by tman on 03/11/16.
//

#ifndef PALLET_DETECTION_PALLETMODELPOINT_H
#define PALLET_DETECTION_PALLETMODELPOINT_H

#include <opencv2/core/core.hpp>
#include <stdlib.h>
#include <yaml-cpp/yaml.h>
#include <ros/common.h>
#include "MatUtils.h"
using namespace std;
using namespace cv;

/**
 * \brief A class storing point groups
 *
 * This class stores a point group and computes the distance to a reference point
 */
class PalletModelPointGroup {
private:
    Mat point_group_; //!< A Mat holding the points
    Mat ref_point_; //!< The reference point
    Mat dist_to_ref_point_; //!< A Mat storing the point distances with respect to the reference point
    string name_; //!< An identifier of the point group
    /**
     * \brief Computes the 2D point distances to the reference point
     */
    void computeDistances();

public:

    /**
     * \brief Standart constructor
     */
    PalletModelPointGroup();

    /**
     * \brief Constructor
     * @param name the point group indentifier
     */
    PalletModelPointGroup(string name);

    /**
     * \brief Constructor
     * @param points the points of the point group
     * @param ref_point the reference point
     * @param name the point group indentifier
     */
    PalletModelPointGroup(Mat points, Mat ref_point, string name);

    /**
     * \brief Constructor
     * @param points the points of the point group
     * @param name the point group indentifier
     */
    PalletModelPointGroup(Mat points, string name);

    /**
     * \brief Sets the reference point
     * @param point the reference point
     */
    void setRefPoint(Mat point);

    /**
     * \brief Returns the reference point
     *
     * @param point the reference point
     */
    void getRefPoint(Mat & point);

    /**
     * \brief returns the points
     *
     * @param points the Mat holding the points
     */
    void getPointGroup(Mat & points);

    /**
     * \brief Sets the points
     *
     * Sets the member point_group to the new value and recomputes the distance map. If you want a custom refrence point, dont forget to set it.
     *
     * @param points the Mat containing the points of the point group
     */
    void setPointGroup(const Mat points);

    /**
     * \brief Returns the intentifier
     *
     * @param name the identifier
     */
    void getName(string & name);

    /**
     * \brief prints all data in the std output
     */
    void print();

    /**
     * \brief Loads the data from a fiel
     *
     * Loads the point group data from a yaml file.
     * @param file path to the file
     * @param name name of the identifier (key) which holds the data
     * @return returns true if success, and false othervise
     */
    bool Load (std::string file, std::string name);



};


#endif //PALLET_DETECTION_PALLETMODELPOINT_H
