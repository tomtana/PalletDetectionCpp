//
// Created by tman on 3/21/17.
//

#ifndef PALLET_DETECTION_PCLOPENCVBRIDGE_H
#define PALLET_DETECTION_PCLOPENCVBRIDGE_H
#include <pcl/point_types.h>
#include <opencv2/core/mat.hpp>
#include <pcl/point_cloud.h>


class PclOpenCvBridge {
public:
    PclOpenCvBridge();
    /**
     * Deep Copy of the cv Mat to Create a PointCloud
     * @param cv_pcl 3 channel CV_32FC3 Mat
     * @return pointcloud with PointXYZ type
     */
    static pcl::PointCloud<pcl::PointXYZ>::Ptr MatCopyToPointXYZ(cv::Mat cv_pcl);
    /**
     * Deep Copy of the cv Mat to Create a PointCloud with intensity values
     * @param cv_pcl 3 channel CV_32FC3 Mat
     * @param img_left left image intensity values in CV_32FC1, as far as experiments showed, it doesnt matter if normalized to 255, or 1, but
     *          in case that in a later stage some other colors should be added it could come to complications. Thus, we agree now to the convention to pass normalized values with in the rage of [0,1]
     * @return pointcloud with PointXYZ type
     */
    static pcl::PointCloud<pcl::PointXYZI>::Ptr MatCopyToPointXYZI(cv::Mat cv_pcl, cv::Mat img_left);
};


#endif //PALLET_DETECTION_PCLOPENCVBRIDGE_H
