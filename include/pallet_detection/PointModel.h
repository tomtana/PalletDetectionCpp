//
// Created by tman on 29/11/16.
//

#ifndef PALLET_DETECTION_POINTMODEL_H
#define PALLET_DETECTION_POINTMODEL_H

#include "PPoints.h"

class PointModel {
public:
    std::vector<PPoints2d > points2_;
    cv::Mat point_dist_;
    PointModel();
    PointModel(std::vector<std::vector<cv::Point2d > > points);
    PointModel(std::vector<PPoints2d > points);
    cv::Mat getPoints2();
    virtual bool load(std::string path, std::string key);
    virtual bool save(std::string path, std::string key);
    virtual bool computeDistances();
    /**
     * \brief Returns the angles of all the line elements of the model
     *
     * The model in this particular usage represents for each PPoints element points lying on one line. This function
     * computed for each element of "vector<PPoints3<double> > points3_" the angle with respect to the x axis
     * @return A vector of angles
     */
    std::vector<double> getAngles();
    /**
     *
     *
     * interpretes the points of each element of the vector std::vector<PPoints2d > points2_ as lines and computes their distance at the position x_at
     * @param idx_1 index of the first element to be interpreted as line
     * @param idx_2 index of the second element to be interpreted as line
     * @param x_at the x position in the image at which the distance should be computed
     * @return the distance
     */

    float getDistOf(int idx_1,int idx_2, int x_at);
};


#endif //PALLET_DETECTION_POINTMODEL_H
