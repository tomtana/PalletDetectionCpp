//
// Created by tman on 12/1/16.
//

#ifndef PALLET_DETECTION_UNITTEST_H
#define PALLET_DETECTION_UNITTEST_H
#include <PPoints3.h>
#include "PPoints.h"
#include <MatUtils.h>
#include <stdlib.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>



class UnitTest {
public:
    UnitTest();
    static void TestPPoints();
    static void TestPPoints3();
    static void TestPPoint();
    static void TestPPoint3();
    static void TestPalletModel(image_geometry::PinholeCameraModel& cam_model);
    static void TestLineUtils();
    static void TestLineUtils2();
    static void Test();
};


#endif //PALLET_DETECTION_UNITTEST_H
