#include <stdexcept>//
// Created by tman on 16/11/16.
//

#include "PalletModel_old.h"

PalletModel::PalletModel(){

}
PalletModel::PalletModel(vector<PalletModelPointGroup> point_groups){
    if (!point_groups.empty()) {
        point_groups_ = point_groups;
    } else{
        __throw_runtime_error("Point Group was empty");
    }
}
//sets the 3d transformation
void PalletModel::setTransformation3D(const Mat& T){
    if (!T.empty()) {
        trans_3d_=T.clone();
    } else{
        cout<<"Transformation matrix T is empty"<<endl;
    }
}
//sets the point group member object
void PalletModel::setPointGroups(vector<PalletModelPointGroup> point_groups){
    point_groups_=point_groups;
    //try to perform tranformation of the point group
    transformPointGroup();

}
//adds one point group to the existing vector
void PalletModel::addPointGroup(PalletModelPointGroup point_group){
    if(point_groups_.empty()){
        cout<<"PalletModel::addPointGroup: No point group vector initialized yet"<<endl;
        return;
    }
    point_groups_.push_back(point_group);
}
//gives the points from all the point groups in one mat object
void PalletModel::getAllPoints(Mat& points){
    //check fist if a transformed point group is available
    if(point_groups_.empty()){
        cout<<"PalletModel::getAllPoints: No transformed point group available."<<endl;
        return;
    }
    //copy first element
    Mat p;
    point_groups_[0].getPointGroup(p);
    //concacenate the rest
    for(int i=1;i<point_groups_.size();i++){
        Mat t;
        point_groups_[i].getPointGroup(t);
        hconcat(p, t, p);
    }
    points=p;
}
//gives the points from all the 3d transformed point groups in one mat object
void PalletModel::getAllPointsTransf(Mat& points){
    //check fist if a transformed point group is available
    if(point_groups_transf_.empty()){
        cout<<"PalletModel::getAllPointsTransf: No transformed point group available."<<endl;
        return;
    }
    //copy first element
    Mat p;
    point_groups_transf_[0].getPointGroup(p);
    //concacenate the rest
    for(int i=1;i<point_groups_transf_.size();i++){
        Mat t;
        point_groups_transf_[i].getPointGroup(t);
        hconcat(p, t, p);
    }
    points=p;
}

void PalletModel::getAllPointsTransf2D(Mat& points){
    //check fist if a transformed point group is available
    if(point_groups_transf_2D_.empty()){
        cout<<"PalletModel::getAllPointsTransf: No transformed point group available."<<endl;
        return;
    }
    //copy first element
    Mat p;
    point_groups_transf_2D_[0].getPointGroup(p);
    //concacenate the rest
    for(int i=1;i<point_groups_transf_2D_.size();i++){
        Mat t;
        point_groups_transf_2D_[i].getPointGroup(t);
        hconcat(p, t, p);
    }
    points=p;
}


//get all the point groups
vector<PalletModelPointGroup> PalletModel::getPointGroups(){
    return point_groups_;
}

vector<PalletModelPointGroup> PalletModel::getPointGroupsTransf2D(){
    return point_groups_transf_2D_;
}

//get the point groups which has been transformed by the transformation matrix.
vector<PalletModelPointGroup> PalletModel::getPointGroupsTransf(){
    return point_groups_transf_;
}
//loads the model from a file
bool PalletModel::Load (string file){
    //load the file
    YAML::Node baseNode = YAML::LoadFile(file);
    if (baseNode.IsNull()){
        return false; //File Not Found?
    }
    vector<PalletModelPointGroup> point_groups;
    for (YAML::const_iterator it=baseNode.begin(); it!=baseNode.end();++it) {
        YAML::Node key = it->first;
        if (key.Type() == YAML::NodeType::Scalar) {
            string name=key.as<std::string>();
            PalletModelPointGroup pg;
            pg.Load(file,name);
            point_groups.push_back(pg);
        }
    }
    /*for (int i=0;i<point_groups.size();i++){
        point_groups[i].print();
    }*/
    point_groups_=point_groups;
    transformPointGroup();
}

bool PalletModel::transformPointGroup(){
    //check if transformation is set
    if(trans_3d_.empty()){
        //__throw_runtime_error("PalletModel::transformPointGroup: No transformation is set");
        return false;
    }
    //check if pointgroup vector is set
    if(point_groups_.empty()){
        //__throw_runtime_error("PalletModel::transformPointGroup: Point group vector is empty");
        return false;
    }
    //get transformation
    Mat t=trans_3d_.clone();

    //for each point group perform transformation and projection
    vector<PalletModelPointGroup> point_groups_transf;
    for(int i=0;i<point_groups_.size();i++){
        PalletModelPointGroup pg=point_groups_[i];
        Mat p;
        Mat pp;
        pg.getPointGroup(p);
        MatUtils::convertPointsToHomogeneous(p,p);
        //perform transformation
        p=t*p;
        pp=p.clone();
        //todo here perform projection
        //convert to cartesian
        MatUtils::convertPointsFromHomogeneous(p,p);
        //store the transformed points in the point group
        pg.setPointGroup(p);

        point_groups_transf.push_back(pg);

    }
    point_groups_transf_=point_groups_transf;

    //perform transformation




}