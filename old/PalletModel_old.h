//
// Created by tman on 16/11/16.
//

#ifndef PALLET_DETECTION_PALLETMODEL_H
#define PALLET_DETECTION_PALLETMODEL_H

#include "PalletModelPointGroup.h"
#include <stdlib.h>
#include <opencv2/core/core.hpp>
#include <yaml-cpp/yaml.h>
#include "MatUtils.h"
#include <image_geometry/pinhole_camera_model.h>


using namespace std;
using namespace cv;
/**
 * \brief The pallet model class
 *
 * This class holds all information defining the model of the pallet. The model is constructed out for corner points.
 */
class PalletModel {
private:

    vector<PalletModelPointGroup> point_groups_; //!< Holds the point groups which store the corner points.
    vector<PalletModelPointGroup> point_groups_transf_; //!< Holds the point groups which store the transformed corner points.
    vector<PalletModelPointGroup> point_groups_transf_2D_; //!< Holds the in the camera projected point groups.
    Mat trans_3d_; //!< The homogeneous transformation matrix
    //image_geometry::PinholeCameraModel&  ros_cam_model_; //!< refrence to the camera calibration parameters

public:
    \

    /**
     * \brief Standart constructor
     *
     * @return
     */
     PalletModel();


    /**
     * \brief A constructor
     *
     * Constructor which takes the pallet model points
     *
     * @param point_groups A vector with all the model corner points
     * @param ros_cam_model A reference to the ros camera model class
     * @return
     */
    PalletModel(vector<PalletModelPointGroup> point_groups);


    /** \brief Sets the transformation matrix
     *
     * Sets the transformatin matrix which is used to transform the pallet model points to the estimated pallet position
     *
     * @param T the 4x4 transformation matrix
     */
    void setTransformation3D(const Mat& T);


    /**
     * \brief Fuction to set the point group holding the model corner points.
     * @param point_groups A vector holding the point group
     */
    void setPointGroups(vector<PalletModelPointGroup> point_groups);


    /**
     * \brief Adds a PalletModelPointGroup instance to the point_group vector. The new element is pushed back.
     * @param point_group The new point group to add.
     */
    void addPointGroup(PalletModelPointGroup point_group);


    /**
     * \brief Returns all points.
     *
     * Returns a Mat containing all corner points defined in the elements of the point_group vector
     * @param points The Mat with all corner points.
     */
    void getAllPoints(Mat& points);


    /**
     * \brief Returns all transformed points.
     *
     * Returns a Mat containing all transformed corner points defined in the elements of the point_group_tranf vector
     * @param points The Mat with all transformed corner points.
     */
    void getAllPointsTransf(Mat& points);

    /**
     * \brief Returns all transformed points.
     *
     * Returns a Mat containing all transformed and projected corner 2D points defined in the elements of the point_group_tranf_2d_ vector
     * @param points The Mat with all transformed corner points.
     */
    void getAllPointsTransf2D(Mat& points);


    /**
     * \brief returns the point_group vector
     * @return the point group vector
     */
    vector<PalletModelPointGroup> getPointGroups();


    /**
     * \brief returns the point_group_transf vector
     *
     * @return the point group transf vector
     */
    vector<PalletModelPointGroup> getPointGroupsTransf();

    /**
     * \brief returns the point_group_transf_2D_ vector
     *
     * @return the point group transf 2d vector
     */
    vector<PalletModelPointGroup> getPointGroupsTransf2D();

    /**
     * \brief transforms the point groups
     *
     * Transforms all the corner points contained in the corner point_groups with the transformation matrix
     *
     * @return
     */
    bool transformPointGroup();


    /**
     * \brief loads the model from a file
     *
     * Parses the model data from a yaml file specified as string.
     * @param file the file location
     * @return
     */
    bool Load (string file);
};


#endif //PALLET_DETECTION_PALLETMODEL_H
