//
// Created by tman on 3/1/17.
//

#include "CVMorphology.h"
#include <iostream>
#include <stdlib.h>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;


CVMorphology::CVMorphology() {

}


cv::Mat CVMorphology::reconstructByDilation(const cv::Mat &marker_img,const cv::Mat &mask_img,unsigned int iter ,bool full_connectivity) {
    if(marker_img.empty()){
        __throw_runtime_error("CVMorphology::reconstructByDilation: marker_img is empty!");
    }
    if(mask_img.empty()){
        __throw_runtime_error("CVMorphology::reconstructByDilation: mask_img is empty!");
    }
    int structuring_element;
    if(full_connectivity){
        structuring_element=MORPH_RECT;
    } else{
        structuring_element=MORPH_CROSS;
    }
    //create structuring element
    Mat element= getStructuringElement(structuring_element,Size(3,3));

    // Perform morphological reconstruction
    Mat recon1 = marker_img.clone();
    Mat recon1_old = Mat::zeros(recon1.rows,recon1.cols, CV_8UC1);
    int cnt=0;
    while(sum(recon1 - recon1_old)[0] != 0 && (cnt<iter || iter==0)){
        // Retain output of previous iteration
        recon1_old = recon1.clone(); //todo maybe clone

        // Perform dilation
        dilate(recon1, recon1, element);

        // Restrict the dilated values using the mask
        Mat bw = (recon1> mask_img);
        mask_img.copyTo(recon1,bw);
        cnt++;
    }
    //cout<<"number of iterations: "<<cnt<<endl;
    return recon1;
}

cv::Mat CVMorphology::getLine(double angle, double length){
    if(length<2){
        CV_Error(2,"Length must be >=2");
    }else{
        length=length-1;
    }
    //origin
    double o_x=0;
    double o_y=0;
    //point
    double p_x=cos(angle/180*CV_PI)*length;
    double p_y=sin(angle/180*CV_PI)*length;
    //normed vector
    double d_x=p_x/(sqrt(p_x*p_x+p_y*p_y));
    double d_y=p_y/(sqrt(p_x*p_x+p_y*p_y));

    //check which component is bigger
    if(abs(d_x>=abs(d_y))){
        p_x=length;
        double landa=p_x/d_x;
        p_y=landa*d_y;
        //if negative correct with offset
        if(p_y<0){
            o_y=abs(p_y);
            p_y=0;
        }
    }
    else{
        p_y=length;
        double landa=p_y/d_y;
        p_x=landa*d_x;
        //if negative correct with offset
        if(p_x<0){
            o_x=abs(p_x);
            p_x=0;
        }
    }

    //create mat with odd row and cols
    o_x=round(o_x);
    o_y=round(o_y);
    //cout<<"Origin: ["<<o_x<<","<<o_y<<"]"<<endl;
    p_x=round(p_x);
    p_y=round(p_y);
    //cout<<"Point: ["<<p_x<<","<<p_y<<"]"<<endl;
    int rows= static_cast<int>(max(o_y,p_y))+1;
    int cols= static_cast<int>(max(o_x,p_x))+1;
    rows=(rows%2!=0)?rows:rows+1;
    cols=(cols%2!=0)?cols:cols+1;
    //cout<<"rows: "<<rows<<"\ncols: "<<cols<<endl;
    Mat line_mat=Mat::zeros(rows,cols,CV_8UC1);
    line( line_mat, Point(static_cast<int>(o_x), static_cast<int>(o_y)), Point(static_cast<int>(p_x), static_cast<int>(p_y)), Scalar(255,0,0), 1,8);
    return line_mat;
}