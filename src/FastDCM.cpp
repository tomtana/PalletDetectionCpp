//
// Created by tman on 3/2/17.
//

#include "FastDCM.h"
#include <iostream>

//todo create yaml config file and load it in this function
FastDCM::FastDCM(string path) {

}

FastDCM::FastDCM() {
    // Load configuration
    lm.nDirections_ = 30; //number directions
    lm.directionCost_ = 0.5; //directional cost
    lm.maxCost_ = 30; //maximum edge cost
    lm.scale_ = 1.0; //matching scale
    lm.db_scale_ = 1.0; //template scale
    lm.baseSearchScale_ = 1.0;
    lm.minSearchScale_ = 1.0;
    lm.maxSearchScale_ = 1.0;
    lm.baseSearchAspect_ = 1.0;
    lm.minSearchAspect_ = 1.0;
    lm.maxSearchAspect_ = 1.0;
    lm.searchStepSize_ = 2;
    lm.searchBoundarySize_ = 2;
    lm.minCostRatio_ = 1.0;
    //lf_conf.dump();
}

bool FastDCM::performDetection(cv::Mat img, const std::vector<cv::Mat> & templates, double confidence,
                               vector<vector<LMDetWind> > & result) {

    if(img.empty()){
        __throw_runtime_error("FastDCM::performDetection: img is empty");
    }
    if(templates.size()==0){
        __throw_runtime_error("FastDCM::performDetection: img is empty");
    }
    //init number of templates
    lm.ndbImages_=templates.size();
    lm.dbImages_ = new EIEdgeImage[lm.ndbImages_];

    //line fitter
    LFLineFitter lf,lf_templ[templates.size()];

    //images
    Image<uchar> chamf_input(img);
    std::vector<Image<uchar> > chamf_templates;
    //init line fitters
    lf.Configure(lf_conf.sigma_fit_a_line,
                 lf_conf.sigma_find_support,
                 lf_conf.max_gap,
                 lf_conf.nLayer,
                 lf_conf.nLinesToFitInStage,
                 lf_conf.nTrialsPerLineInStage);
    lf.Init();
    lf.FitLine(&chamf_input);


    //init templates in the detection
    for(int i=0;i<templates.size();i++){
        if(templates[i].empty()){
            __throw_runtime_error("FastDCM::performDetection: templates vector constains empty mat");
        }
        if(templates[i].channels()!=1){
            __throw_runtime_error("FastDCM::performDetection: templates have to be a one channel Mat");
        }
        if(templates[i].type()!=CV_8UC1){
            __throw_runtime_error("FastDCM::performDetection: templates have to be of type CV_8UC1 Mat");
        }
        //init template vector of Image types
        chamf_templates.push_back(Image<uchar>(templates[i]));
        //init the line fitter
        lf_templ[i].Configure(lf_conf.sigma_fit_a_line,
                     lf_conf.sigma_find_support,
                     lf_conf.max_gap,
                     lf_conf.nLayer,
                     lf_conf.nLinesToFitInStage,
                     lf_conf.nTrialsPerLineInStage);
        lf_templ[i].Init();
        //associate the Image to the line fitter
        lf_templ[i].FitLine(&chamf_templates[i]);
        //pass templates to line matcher
        lm.dbImages_[i].SetNumDirections(lm.nDirections_);
        lm.dbImages_[i].Read( lf_templ[i] );
        lm.dbImages_[i].Scale(lm.scale_*lm.db_scale_);
    }
    //Now perform template matching
    //int e1,e2;
    //double time;
    //e1 = cv::getTickCount();
    lm.SingleShapeDetectionWithVaryingQuerySize(lf,confidence,result);
    //e2 = cv::getTickCount();
    //time = (e2 - e1)/ cv::getTickFrequency();
    //cout<<"Time: SingleShapeDetectionWit:\t \t \t"<<time<<endl;


    //return false if a a match was found
    if(result.empty())
        return false;
    else
        return true;
}
