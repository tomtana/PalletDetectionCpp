//
// Created by tman on 2/21/17.
//

#include "ITKMorphology.h"
#include <iostream>
#include <typeinfo>
#include <itkImageRegion.h>
#include <itkRegion.h>
#include <opencv2/imgproc.hpp>
#include <itkBinaryReconstructionByDilationImageFilter.h>
#include <itkBinaryErodeImageFilter.h>
#include <itkOpenCVImageBridge.h>
#include <itkOrImageFilter.h>

using namespace std;

ITKMorphology::ITKMorphology() {

}

//type check is propably not needed !
template<typename TImage>
typename TImage::Pointer ITKMorphology::CVtoITK(cv::Mat cv_img) {
    if(cv_img.empty()){
        __throw_runtime_error("ITKMorphology::CVtoITK: cv_mat is empty.");
    }
    //create itk image container and convert cv::Mat to it
    return itk::OpenCVImageBridge::CVMatToITKImage<TImage>(cv_img);


}


template<typename TImage>
cv::Mat ITKMorphology::ITKtoCV(typename TImage::Pointer itk_img) {
    return itk::OpenCVImageBridge::ITKImageToCVMat<TImage>(itk_img);
}

template<typename TImage>
typename TImage::Pointer ITKMorphology::computeErosion(const typename TImage::Pointer image_in, itk::FlatStructuringElement<2> kernel) {
    typedef itk::BinaryErodeImageFilter<TImage,TImage,itk::FlatStructuringElement<2> > erode_filter_type;
    typename erode_filter_type::Pointer erode_filter=erode_filter_type::New();
    erode_filter->SetInput(image_in);
    erode_filter->SetKernel(kernel);
    erode_filter->Update();
    return erode_filter->GetOutput();

}

template<typename TImage>
typename TImage::Pointer ITKMorphology::computeErosionByLines(typename TImage::Pointer image_in, std::vector< std::vector<double> > param_line){
    typename TImage::Pointer itk_img_result= TImage::New();
    //init itk_img result
    typename TImage::RegionType region(image_in->GetBufferedRegion ());
    itk_img_result->SetRegions(region);
    itk_img_result->Allocate();
    itk_img_result->FillBuffer(0);
    for(int i=0;i<param_line.size();i++){
        ItkUint8Img::Pointer itk_img_temp;
        itk_img_temp=computeErosion<TImage>(image_in,ITKMorphology::getLine(param_line[i][0],param_line[i][1]));
        //binary of of the temp image with the result image
        itk_img_result=ITKMorphology::Or<TImage>(itk_img_temp,itk_img_result);
    }
    return itk_img_result;
}

template<typename TImage>
typename TImage::Pointer ITKMorphology::computeReconstructionByDilation(typename TImage::Pointer image_to_reconstr,
                                                    typename TImage::Pointer image_to_compare,
                                                    bool fully_connected) {

    typename itk::BinaryReconstructionByDilationImageFilter<TImage >::Pointer reconstr_filter = itk::BinaryReconstructionByDilationImageFilter<TImage >::New();
    reconstr_filter->SetMaskImage(image_to_compare);
    reconstr_filter->SetFullyConnected(fully_connected);
    reconstr_filter->SetMarkerImage(image_to_reconstr);
    reconstr_filter->Update();
    return reconstr_filter->GetOutput();
}

itk::FlatStructuringElement< 2 > ITKMorphology::getLine(double length, double angle) {
    if(length<2){
        CV_Error(2,"Length must be >=2");
    }else{
        length=length-1;
    }
    //origin
    double o_x=0;
    double o_y=0;
    //point
    double p_x=cos(angle/180*CV_PI)*length;
    double p_y=sin(angle/180*CV_PI)*length;
    //normed vector
    double d_x=p_x/(sqrt(p_x*p_x+p_y*p_y));
    double d_y=p_y/(sqrt(p_x*p_x+p_y*p_y));

    //check which component is bigger
    if(abs(d_x>=abs(d_y))){
        p_x=length;
        double landa=p_x/d_x;
        p_y=landa*d_y;
        //if negative correct with offset
        if(p_y<0){
            o_y=abs(p_y);
            p_y=0;
        }
    }
    else{
        p_y=length;
        double landa=p_y/d_y;
        p_x=landa*d_x;
        //if negative correct with offset
        if(p_x<0){
            o_x=abs(p_x);
            p_x=0;
        }
    }

    //create mat with odd row and cols
    o_x=round(o_x);
    o_y=round(o_y);
    //cout<<"Origin: ["<<o_x<<","<<o_y<<"]"<<endl;
    p_x=round(p_x);
    p_y=round(p_y);
    //cout<<"Point: ["<<p_x<<","<<p_y<<"]"<<endl;
    int rows= static_cast<int>(max(o_y,p_y))+1;
    int cols= static_cast<int>(max(o_x,p_x))+1;
    rows=(rows%2!=0)?rows:rows+1;
    cols=(cols%2!=0)?cols:cols+1;
    //cout<<"rows: "<<rows<<"\ncols: "<<cols<<endl;
    cv::Mat line_mat=cv::Mat::zeros(rows,cols,CV_8UC1);
    cv::line( line_mat, cv::Point(static_cast<int>(o_x), static_cast<int>(o_y)), cv::Point(static_cast<int>(p_x), static_cast<int>(p_y)), cv::Scalar(255,0,0), 1,8);
    itk::FlatStructuringElement< 2 >::ImageType::Pointer structuring_element;
    structuring_element=CVtoITK<ItkBinImg>(line_mat);
    itk::FlatStructuringElement< 2 > kernel = itk::FlatStructuringElement< 2 >::FromImage(structuring_element);
    return kernel;
}



template<typename TImage>
typename TImage::Pointer ITKMorphology::Or(typename TImage::Pointer image1, typename TImage::Pointer image2) {
    typedef itk::OrImageFilter< TImage, TImage, TImage> itkOrType;
    typename itkOrType::Pointer itkOr=itkOrType::New();
    itkOr->SetInput(0,image1);
    //itkOr->SetInput(1,filt_erode_2->GetOutput());
    itkOr->SetInput(1,image2);
    itkOr->Update();
    return itkOr->GetOutput();
}


void
ITKMorphology::openByReconstructionWithLines(const cv::Mat &img_in, cv::Mat &img_out,
                                             std::vector<std::vector<double> > param_line, bool fully_connected) {
    ItkUint8Img::Pointer itk_img_in=ITKMorphology::CVtoITK<ItkUint8Img>(img_in);
    //perform erosion
    ItkUint8Img::Pointer itk_img_eroded = ItkUint8Img::New();
    itk_img_eroded = computeErosionByLines<ItkUint8Img>(itk_img_in,param_line);

    ItkUint8Img::Pointer itk_img_reconstr= ItkUint8Img::New();
    itk_img_reconstr=computeReconstructionByDilation<ItkUint8Img>(itk_img_eroded,itk_img_in,fully_connected);
    img_out=ITKMorphology::ITKtoCV<ItkUint8Img>(itk_img_reconstr);
}


template ItkUint8Img::Pointer ITKMorphology::CVtoITK<ItkUint8Img>(cv::Mat);
template ItkBinImg::Pointer ITKMorphology::CVtoITK<ItkBinImg>(cv::Mat );

template cv::Mat ITKMorphology::ITKtoCV<ItkUint8Img>(ItkUint8Img::Pointer);
template cv::Mat ITKMorphology::ITKtoCV<ItkBinImg>(ItkBinImg::Pointer);

template ItkUint8Img::Pointer ITKMorphology::computeErosion<ItkUint8Img>(ItkUint8Img::Pointer, itk::FlatStructuringElement<2> );
template ItkBinImg::Pointer ITKMorphology::computeErosion<ItkBinImg>(ItkBinImg::Pointer, itk::FlatStructuringElement<2> );

template ItkUint8Img::Pointer ITKMorphology::computeReconstructionByDilation<ItkUint8Img>(ItkUint8Img::Pointer,ItkUint8Img::Pointer,bool );
template ItkBinImg::Pointer ITKMorphology::computeReconstructionByDilation<ItkBinImg>(ItkBinImg::Pointer,ItkBinImg::Pointer,bool );

template ItkUint8Img::Pointer ITKMorphology::Or<ItkUint8Img>(ItkUint8Img::Pointer,ItkUint8Img::Pointer);
template ItkBinImg::Pointer ITKMorphology::Or<ItkBinImg>(ItkBinImg::Pointer,ItkBinImg::Pointer);

template ItkUint8Img::Pointer ITKMorphology::computeErosionByLines<ItkUint8Img>(ItkUint8Img::Pointer, std::vector< std::vector<double> >);

