//
// Created by tman on 12/6/16.
//

#include <opencv2/imgproc/imgproc.hpp>
#include "LineUtils.h"
#include <utility>
#include <vector>
#include <limits>

using namespace std;
using namespace cv;


//todo test
Point2f LineUtils::getIntersection(Vec4f L1, Vec4f L2) {
    float x1, x2, x3, x4, y1, y2, y3, y4;
    x1 = L1[0];
    x2 = L1[2];
    x3 = L2[0];
    x4 = L2[2];
    y1 = L1[1];
    y2 = L1[3];
    y3 = L2[1];
    y4 = L2[3];
    float denominator;
    denominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    /*
    //cout<<"denominator: "<<denominator<<endl;
    if(denominator==0){
        cout<<"LineUtils::getIntersection: Denominator is 0. pay attention!"<<endl;
    }*/
    Point2f point = Point2f(round(((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4) / denominator)),
                            round(
                                    ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / denominator));

    return point;
}

/**
 * Computes all the intersections between the two vectors.
 * @param L1 a line vector with n Point elements
 * @param L2 a line vector with m Point elements
 * @return a two dimensional arraqy with n x m  Point elements
 */
std::vector<std::vector<cv::Point2f> >
LineUtils::getIntersection(std::vector<cv::Vec4f> L1, std::vector<cv::Vec4f> L2) {
    std::vector<std::vector<cv::Point2f> > intersec;
    for (int i = 0; i < L1.size(); ++i) {
        vector<Point2f> p;
        for (int j = 0; j < L2.size(); ++j) {
            p.push_back(getIntersection(L1[i], L2[j]));
        }
        intersec.push_back(p);
    }
    return intersec;
}

//todo test
Vec4f LineUtils::getCartesian(Vec2f line, Rect border) {
    float rho = line[0];
    float theta = line[1];
    Vec4f linep; //line defined through the 2 points
    //cout<<"Theta: "<<round(theta*180.0/CV_PI)<<endl;
    /*
    //1. check if line parallel to the x axis
    if(round(theta*180.0/CV_PI)==90){
        Point2i p(border.x,static_cast<int>(rho));
        if(isInsideRoi(p,border)){
            linep[0]=p.x;
            linep[1]=p.y;
            linep[2]=border.x+border.width;
            linep[3]=static_cast<int>(rho);
        }
        //if line outside border throw error
        else{
            cout << "LineUtils::getCartesian: Be careful! Line not within the defined boarder" << endl;
            CV_Error(1, "Line not within defined boarder");
        }
    }
    //2. check if line parallel to y axis
    else if(round(theta*180.0/CV_PI)==0){
        Point2i p(static_cast<int>(rho),border.y);
        if(isInsideRoi(p,border)){
            linep[0]=p.x;
            linep[1]=p.y;
            linep[2]=static_cast<int>(rho);
            linep[3]=border.y+border.height;
        }
        //if line outside border throw error
        else{
            cout << "LineUtils::getCartesian: Be careful! Line not within the defined boarder" << endl;
            CV_Error(1, "Line not within defined boarder");
        }
    }
    //in all other cases
    else{*/
    //compute all intersections with the limits
    Point2f p_x_min(border.x, getY(line, border.x));
    Point2f p_y_min(getX(line, border.y), border.y);
    Point2f p_x_max(border.x + border.width, getY(line, border.x + border.width));
    Point2f p_y_max(getX(line, border.y + border.height), border.y + border.height);
    //cout<<"p_x_min: "<<p_x_min<<endl;
    //cout<<"p_x_max: "<<p_x_max<<endl;
    //cout<<"p_y_min: "<<p_y_min<<endl;
    //cout<<"p_y_max: "<<p_y_max<<endl;

    //enum to store location of intersection. Left, Right, Top, Bottom of the sides of a rectangle. Where Top is the
    //side with the smaller y value, and Left the side with the smaller x value.
    enum EN_INERSEC {
        LEFT, RIGHT, TOP, BOTTOM, DEFAULT
    } intersec;
    intersec = DEFAULT;

    //check until first intersection within the limits found. store the intersection in the intersec variable.
    if (isInsideRoi(p_x_min, border)) {
        intersec = LEFT;
        linep[0] = p_x_min.x;
        linep[1] = p_x_min.y;
    } else if (isInsideRoi(p_y_min, border)) {
        intersec = TOP;
        linep[0] = p_y_min.x;
        linep[1] = p_y_min.y;
    } else if (isInsideRoi(p_y_max, border)) {
        intersec = BOTTOM;
        linep[0] = p_y_max.x;
        linep[1] = p_y_max.y;
    }//non of them within the boarder
        //at this point it is already clear that the line is not inside the defined limits.
    else {
        //cout << "LineUtils::getCartesian: Be careful! Line not within the defined boarder" << endl;
        //CV_Error(1, "Line not within defined boarder");
        //set the points to nan to indicate that the line is not defined within the limits
        linep[0] = numeric_limits<float>::quiet_NaN();
        linep[1] = numeric_limits<float>::quiet_NaN();

    }
    if (isInsideRoi(p_x_max, border)) {
        linep[2] = p_x_max.x;
        linep[3] = p_x_max.y;
    } else if (isInsideRoi(p_y_max, border) && intersec != BOTTOM) {
        linep[2] = p_y_max.x;
        linep[3] = p_y_max.y;
    } else if (isInsideRoi(p_y_min, border) && intersec != TOP) {
        linep[2] = p_y_min.x;
        linep[3] = p_y_min.y;
    }
        //non of them within the boarder. Could also be packed in the else before.
    else {
        //cout << "LineUtils::getCartesian: Be careful! Line not within the defined boarder" << endl;
        //CV_Error(1, "Line not within defined boarder");
        linep[2] = numeric_limits<float>::quiet_NaN();
        linep[3] = numeric_limits<float>::quiet_NaN();
    }
    //}
    //sort the points according to the x value
    if (linep[0] > linep[2]) {
        Vec4f v = linep;
        linep[0] = v[2];
        linep[1] = v[3];
        linep[2] = v[0];
        linep[3] = v[1];
    }
    return linep;

}

//
Vec4f LineUtils::getCartesian(cv::Vec2f line, int x_min, int x_max, int y_min, int y_max) {
    return getCartesian(line, Rect(x_min, y_min, x_max - x_min, y_max - y_min));
}

vector<Vec4f> LineUtils::getCartesian(vector<Vec2f> lines, Rect border) {
    vector<Vec4f> linesp;
    for (int i = 0; i < lines.size(); ++i) {
        linesp.push_back(getCartesian(lines[i], border));
    }
    return linesp;
}

//todo test
vector<Vec4f> LineUtils::getCartesian(vector<Vec2f> lines, int x_min, int x_max, int y_min, int y_max) {
    return getCartesian(lines, Rect(x_min, y_min, x_max - x_min, y_max - y_min));
}

float LineUtils::getX(cv::Vec2f line, int y) {
    float rho = line[0];
    float theta = line[1];
    //cout<<"LineUtils::getX: theta="<<theta<<endl;
    return static_cast<float>(round(rho / cos(theta) - y * sin(theta) / cos(theta)));
}

float LineUtils::getY(cv::Vec2f line, int x) {
    float rho = line[0];
    float theta = line[1];
    //cout<<"LineUtils::getY: theta="<<theta<<endl;
    return static_cast<float>((round(-cos(theta) / sin(theta) * x + rho / sin(theta))));
}

float LineUtils::getY(cv::Vec4f line, int x) {
    float x1, x2, y1, y2;
    x1 = line[0];
    x2 = line[2];
    y1 = line[1];
    y2 = line[3];
    return static_cast<float>((round(y1 + (x - x1) / (x2 - x1) * (y2 - y1))));
}

float LineUtils::getX(cv::Vec4f line, int y) {
    float x1, x2, y1, y2;
    x1 = line[0];
    x2 = line[2];
    y1 = line[1];
    y2 = line[3];
    return static_cast<float>(round(x1 + (y - y1) / (y2 - y1) * (x2 - x1)));
}

bool LineUtils::isInsideRoi(Point2i point, Rect roi) {
    int x_min = roi.x;
    int x_max = roi.x + roi.width;
    int y_min = roi.y;
    int y_max = roi.y + roi.height;
    if ((point.x >= x_min && point.x <= x_max) && (point.y >= y_min && point.y <= y_max)) {
        return true;
    } else {
        //cout<<"Point: "<<point<<" v"<<x_min<<x_max<<y_min<<y_max<<endl;
        return false;
    }
}

//todo test
//computes the angle of a line with respect to the x axis, the resulting angle is all the time expressed in the smaller angle and the range is [-90,90]
double LineUtils::computeAngle(cv::Vec4f line) {
    Vec2f vec(line[2] - line[0], line[3] - line[1]);
    return atan(vec[1] / vec[0]);
}

//todo test, especially the limits around 90,-90, and around 0
std::vector<cv::Vec4f>
LineUtils::filterLinesByAngle(std::vector<cv::Vec4f> lines, float angle, float angle_derivation) {
    std::vector<cv::Vec4f> lines_f;
    //convert angle to rad
    for (int i = 0; i < lines.size(); ++i) {
        double ang = LineUtils::computeAngle(lines[i]);
        //if angle is different from the given angle or different from the angle +180 degree (==PI)
        //this is because the angle is cyclic and the transition between 0 and 180 needs to be considered
        if (abs(ang - angle) <= angle_derivation || abs(ang - angle + CV_PI) <= angle_derivation) {
            lines_f.push_back(lines[i]);
        }
    }
    return lines_f;
}

std::vector<cv::Vec4f>
LineUtils::filterLinesByAngleDeg(std::vector<cv::Vec4f> lines, float angle, float angle_derivation) {
    return filterLinesByAngle(lines, angle * CV_PI / 180, angle_derivation * CV_PI / 180);
}

cv::Vec4f LineUtils::getIntersectionWithRoi(cv::Vec4f line, cv::Rect roi) {
    Vec4f linep; //line defined through the 2 points

    //compute all intersections with the limits
    Point2f p_x_min(roi.x, getY(line, roi.x));
    Point2f p_y_min(getX(line, roi.y), roi.y);
    Point2f p_x_max(roi.x + roi.width, getY(line, roi.x + roi.width));
    Point2f p_y_max(getX(line, roi.y + roi.height), roi.y + roi.height);
    //cout<<"p_x_min: "<<p_x_min<<endl;
    //cout<<"p_x_max: "<<p_x_max<<endl;
    //cout<<"p_y_min: "<<p_y_min<<endl;
    //cout<<"p_y_max: "<<p_y_max<<endl;

    //enum to store location of intersection. Left, Right, Top, Bottom of the sides of a rectangle. Where Top is the
    //side with the smaller y value, and Left the side with the smaller x value.
    enum EN_INERSEC {
        LEFT, RIGHT, TOP, BOTTOM, DEFAULT
    } intersec;
    intersec = DEFAULT;

    //check until first intersection within the limits found. store the intersection in the intersec variable.
    if (isInsideRoi(p_x_min, roi)) {
        intersec = LEFT;
        linep[0] = p_x_min.x;
        linep[1] = p_x_min.y;
    } else if (isInsideRoi(p_y_min, roi)) {
        intersec = TOP;
        linep[0] = p_y_min.x;
        linep[1] = p_y_min.y;
    } else if (isInsideRoi(p_y_max, roi)) {
        intersec = BOTTOM;
        linep[0] = p_y_max.x;
        linep[1] = p_y_max.y;
    }//non of them within the boarder
        //at this point it is already clear that the line is not inside the defined limits.
    else {
        //cout << "LineUtils::getCartesian: Be careful! Line not within the defined boarder" << endl;
        //CV_Error(1, "Line not within defined boarder");
        //set the points to nan to indicate that the line is not defined within the limits
        linep[0] = numeric_limits<float>::quiet_NaN();
        linep[1] = numeric_limits<float>::quiet_NaN();

    }
    if (isInsideRoi(p_x_max, roi)) {
        linep[2] = p_x_max.x;
        linep[3] = p_x_max.y;
    } else if (isInsideRoi(p_y_max, roi) && intersec != BOTTOM) {
        linep[2] = p_y_max.x;
        linep[3] = p_y_max.y;
    } else if (isInsideRoi(p_y_min, roi) && intersec != TOP) {
        linep[2] = p_y_min.x;
        linep[3] = p_y_min.y;
    }
        //non of them within the boarder. Could also be packed in the else before.
    else {
        //cout << "LineUtils::getCartesian: Be careful! Line not within the defined boarder" << endl;
        //CV_Error(1, "Line not within defined boarder");
        linep[2] = numeric_limits<float>::quiet_NaN();
        linep[3] = numeric_limits<float>::quiet_NaN();
    }
    //}
    //sort the points according to the x value
    if (linep[0] > linep[2]) {
        Vec4f v = linep;
        linep[0] = v[2];
        linep[1] = v[3];
        linep[2] = v[0];
        linep[3] = v[1];
    }
    return linep;
}

cv::Vec4f LineUtils::getIntersectionWithRoi(cv::Vec4f line, int x_min, int x_max, int y_min, int y_max) {
    return getIntersectionWithRoi(line, Rect(x_min, y_min, x_max - x_min, y_max - y_min));
}

std::vector<cv::Vec4f> LineUtils::getIntersectionWithRoi(std::vector<cv::Vec4f> lines, cv::Rect roi) {
    std::vector<cv::Vec4f> l;
    for (int i = 0; i < lines.size(); ++i) {
        l.push_back(getIntersectionWithRoi(lines[i], roi));
    }
    return l;
}

std::vector<cv::Vec4f>
LineUtils::getIntersectionWithRoi(std::vector<cv::Vec4f> lines, int x_min, int x_max, int y_min, int y_max) {
    return getIntersectionWithRoi(lines, Rect(x_min, y_min, x_max - x_min, y_max - y_min));
}


Rect LineUtils::BoundingRect(vector<Vec4f> lines) {
    std::vector<Point2f> points;
    for (int i = 0; i < lines.size(); ++i) {
        points.push_back(Point2f(lines[i][0], lines[i][1]));
        points.push_back(Point2f(lines[i][2], lines[i][3]));
    }
    //get bounding box and correct behaviour. Desired is that extreme points lie directly on the bounding box
    Rect rect = boundingRect(points);
    if (rect.height > 0) {
        rect.height = rect.height - 1;
    } else if (rect.height < 0) {
        //rect.height=rect.height+1;
        CV_Error(1, "LineUtils::BoundingRect: Height of rectangle is negative");
    }
    if (rect.width > 0) {
        rect.width = rect.width - 1;
    } else if (rect.width < 0) {
        //rect.width=rect.width+1;
        CV_Error(1, "LineUtils::BoundingRect: Width of rectangle is negative");
    }

    return rect;
}

// The Rect is converted to a Vec4f where the elements are defined as following Vec4f=[x_min,y_min,x_max,y_max]
cv::Vec4f LineUtils::RectToVec(Rect rect) {
    return Vec4f(rect.x, rect.y, rect.x + rect.width, rect.y + rect.height);
}

float LineUtils::getHorizDist(cv::Vec4f L1, cv::Vec4f L2, float x) {
    return (abs(getY(L2, round(x)) - getY(L1, round(x))));
}


float LineUtils::getVerticDist(cv::Vec4f L1, cv::Vec4f L2, float y) {
    return (abs(getX(L2, round(y)) - getX(L1, round(y))));
}

//todo test result and performance
vector<vector<Vec4f> > LineUtils::filterHorizDist(vector<Vec4f> lines, float dist, float dist_derivation, float x, float angle_diff, int max_x_dist) {
    vector<float> y;
    vector<vector<Vec4f> > hlines;
    vector<int> doublicate_list;
    vector<Vec2i> match_list;
    //check if dist values are valid
    CV_Assert(dist >= dist_derivation);

    //compute y values at x
    for (int i = 0; i < lines.size(); i++) {
        y.push_back(getY(lines[i], x));
    }
    //compute all horizontal distances
    for (int m = 0; m < y.size(); m++) {

        for (int n = 0; n < m; n++) {

            //compute distance
            float d = abs(y[m] - y[n]);
            //cout << d << endl;
            //in case the distance is 0 check if the lines have the same parameters, which would mean that they are identical
            if (round(d) == 0) {
                Point2f intersec = getIntersection(lines[m], lines[n]);
                //in the case if their intersection is not coinciding with the point where their distance is 0, they are parallel -> the same line
                if (round(intersec.x) != x || round(intersec.y) != y[m]) {
                    //check if idx is already represented in the list
                    if (!isInList(doublicate_list, m)) {
                        //cout<<"m: "<<m<<"   n: "<<n<<endl;
                        //doublicate_list.push_back(m);
                    }
                }
            }//if d is Nan indicate error
            else if (d != d) {
                CV_Error(1, "d is NaN, debug here.");
            }//check if dist is in the range
            else if (abs(d - dist) <= dist_derivation) {
                //check if the horizontal lines are close enough to each other to represent the same structure
                //if(abs(lines[m][0]-lines[n][0])<= max_x_dist || abs(lines[m][2]-lines[n][2])<= max_x_dist) {
                float xa1,xa2,xb1,xb2;
                xa1=lines[m][0];
                xa2=lines[m][2];
                xb1=lines[n][0];
                xb2=lines[n][2];
                float x_line_dist=0;
                //check if the lines are not directly intersecting in case we would draw a horizontal line to both of the endpoints
                // line:1 ------------
                // line:2             <-x dist ->_________________
                if(xa1>xb1&&xa1>xb2&&xa2>xb1&&xa2>xb2
                        || xa1<xb1&&xa1<xb2&&xa2<xb1&&xa2<xb2){
                    //create vector with all distances of the line endpoints and compute the minimum
                    vector<float> v;
                    v.push_back(abs(lines[m][0]-lines[n][2]));
                    v.push_back(abs(lines[m][2]-lines[n][0]));
                    v.push_back(abs(lines[m][0]-lines[n][0]));
                    v.push_back(abs(lines[m][2]-lines[n][2]));
                    x_line_dist=*(std::min_element(v.begin(), v.end()));
                    x_line_dist=round(x_line_dist);
                }
                    //in all other cases the vertical line distance is 0
                else{
                    x_line_dist=0;
                }
                    if(x_line_dist<= max_x_dist) {
                        //check if the angle between the two lines is satisfying our angle diff constraint
                        if (LineUtils::areInAngleRange(lines[m], lines[n], angle_diff)) {
                            //cout << "dist diff= " << d << "-" << dist << "=" << abs(d - dist) << endl;
                            match_list.push_back(Vec2i(m, n));
                        }
                    }

            }

        }
    }
    /*
    //print duplicate list
    cout<<"duplicate list:  size: "<<doublicate_list.size()<<endl;
    for (int j = 0; j < doublicate_list.size(); ++j) {
        cout<<doublicate_list[j]<<endl;
    }
    cout<<"match list:  size: "<<match_list.size()<<endl;
    for (int l = 0; l < match_list.size(); ++l) {
        cout<<match_list[l]<<endl;
    }
    */

    //now remove double matches from the list, stored in the duplicate list
    vector<Vec2i>::iterator first = match_list.begin();
    vector<Vec2i>::iterator last = match_list.end();

    //run loop until iterators are equal
    while (first != last) {
        for (int i = 0; i < doublicate_list.size(); i++) {
            if (((*last)[0] == doublicate_list[i]) || ((*last)[1] == doublicate_list[i])) {
                *last=match_list.back();
                match_list.pop_back();
                //*result = *last;
                //--result;
                break;
            }

        }
        --last;
    }
    cout<<"match list:  size: "<<match_list.size()<<endl;
    /*

    for (int l = 0; l < match_list.size(); ++l) {
        cout<<match_list[l]<<endl;
    }
     */

    //create output vector
    for (int k = 0; k < match_list.size(); k++) {
        vector<Vec4f> v;
        v.push_back(lines[match_list[k][0]]);
        v.push_back(lines[match_list[k][1]]);
        hlines.push_back(v);
    }

    return hlines;
}

bool LineUtils::isInList(std::vector<int> list, int i) {
    for (int j = 0; j < list.size(); ++j) {
        if (list[j] == i) {
            return true;
        }
    }
    return false;
}

bool LineUtils::areInAngleRange(cv::Vec4f L1,cv::Vec4f L2, float angle_diff){
    float angle1=LineUtils::computeAngle(L1);
    float angle2=LineUtils::computeAngle(L2);
    return (abs(angle1 - angle2) <= angle_diff || abs(angle1 - angle2 + CV_PI) <= angle_diff);

}

template<typename T_target, typename T_origin, int n>
std::vector<cv::Vec<T_target, n> > LineUtils::convertVecType(std::vector<cv::Vec<T_origin, n> > input) {
    vector<Vec<T_target, n> > out;
    for (int i = 0; i < input.size(); ++i) {
        out.push_back(input[i]);
    }
    return out;
};




/*
template <typename T,int n>void LineUtils::sortVec(std::vector<cv::Vec<T, n> > &vec) {
    std::sort(vec.begin(), vec.end(), [](cv::Vec<T, n>  a, cv::Vec<T, n> b) {
        return b[0] < a[0];
    });
}
*/


//explicit instantiation of the template function
template std::vector<cv::Vec<float, 4> > LineUtils::convertVecType<float, int, 4>(std::vector<cv::Vec<int, 4> > input);

template std::vector<cv::Vec<double, 4> >
LineUtils::convertVecType<double, int, 4>(std::vector<cv::Vec<int, 4> > input);

template std::vector<cv::Vec<int, 4> > LineUtils::convertVecType<int, float, 4>(std::vector<cv::Vec<float, 4> > input);

template std::vector<cv::Vec<int, 4> >
LineUtils::convertVecType<int, double, 4>(std::vector<cv::Vec<double, 4> > input);

/*
template void LineUtils::sortVec(std::vector<cv::Vec<float, 4> > &vec);
template void LineUtils::sortVec(std::vector<cv::Vec<double, 4> > &vec);
template void LineUtils::sortVec(std::vector<cv::Vec<int, 4> > &vec);
 */