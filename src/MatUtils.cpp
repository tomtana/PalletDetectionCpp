//
// Created by tman on 11/30/16.
//
#include "MatUtils.h"
#include <tf/LinearMath/Scalar.h>


using namespace cv;
using namespace std;


void MatUtils::convertPointsToHomogeneous(Mat input,Mat & output){
    if (input.empty()){
        __throw_runtime_error("MatUtils::convertPointsToHomogeneous: Input mat empty");
        return;
    }
    Mat new_row;
    new_row = Mat::ones(1, input.cols, input.type());
    output=input.clone();
    output.push_back(new_row);
}

//todo test function
void MatUtils::convertPointsFromHomogeneous(Mat input, Mat& output){
    Mat m=Mat(input.rows-1,input.cols,input.type());
    for (int i=0; i< input.cols;i++){
        Mat t;
        Rect roi=Rect(i, 0, 1, input.rows-1);
        t=input(roi).clone();
        double divisor;
        switch(input.type())
        {
            case CV_8UC1:
                t=t/input.at<char>(input.rows-1, i);
                break;
            case CV_16S:
                divisor=input.at<short>(input.rows-1, i);
                t=t/divisor;
                break;
            case CV_16U:
                divisor=input.at<short>(input.rows-1, i);
                t=t/divisor;
                break;
            case CV_32SC1:
                divisor=input.at<int>(input.rows-1, i);
                t=t/divisor;
                break;
            case CV_32FC1:
                divisor=input.at<float>(input.rows-1, i);
                t=t/divisor;
                break;
            case CV_64FC1:
                t=t/input.at<double>(input.rows-1, i);
                break;
                // and so on
            default:
                __throw_runtime_error("MatUtils::convertPointsFromHomogeneous: Type of input mat not defined");
        }
        Mat msub=m.col(i);
        t.copyTo(msub);
    }
    output=m;
}
//todo test this function with different datatypes. The mat solution looks risky
void MatUtils::computePointDistances(const Mat points, const Mat ref_points, Mat &distances) {
    if (points.empty() || ref_points.empty()){
        __throw_runtime_error("MatUtils::computePointDistances: One of the input matrices is emtpy");
        return;
    }
    if (points.rows != ref_points.rows){
        __throw_runtime_error("MatUtils::computePointDistances: points and ref points not of same dimension");
        return;
    }
    Mat d;
    d = Mat::zeros(points.cols, ref_points.cols, CV_64FC1);
    //compute the distances
    for(int i=0;i<ref_points.cols;i++){
        for(int j=0;j<points.cols;j++){
            Mat p = points.col(j);
            Mat rp = ref_points.col(i);
            double dist = norm(p, rp);//compute L2 Norm from the difference = distance
            d.at<double>(i, j) = dist;
        }
    }
    distances=d;
}

//todo test function
template<typename T> PPoints3<T> MatUtils::MatToPPoint3(const cv::Mat& m_point) {
//check if mat is empty
    if (m_point.empty()) {
        std::__throw_runtime_error("MatUtils::MatToPoint: Input matrix is empty");
    }

//check if the number of rows is 3
    if(m_point.rows!=3) {
        std::__throw_runtime_error("MatUtils::MatToPoint: Input matrix has to have exactly 3 rows.");
    }

    //copy matrix to points
    std::vector<PPoint3<T> > points;
    T x, y, z;
    for (int n = 0; n < m_point.cols; n++) {
        //find the datatype of the matrix
        switch (m_point.type()) {
            case CV_8UC1:
                x = m_point.at<char>(0, n);
                y = m_point.at<char>(1, n);
                z = m_point.at<char>(2, n);
                break;
            case CV_32SC1:
                x = m_point.at<int>(0, n);
                y = m_point.at<int>(1, n);
                z = m_point.at<int>(2, n);
            case CV_32FC1:
                x = m_point.at<float>(0, n);
                y = m_point.at<float>(1, n);
                z = m_point.at<float>(2, n);
                break;
            case CV_64FC1:
                x = m_point.at<double>(0, n);
                y = m_point.at<double>(1, n);
                z = m_point.at<double>(2, n);
                break;
                // and so on
            default:
                std::__throw_runtime_error("MatUtils::MatToPPoint3: Type of input mat not supported");
        }
        //check the type


        points.push_back(PPoint3<T>(x, y, z));
    }
    return points;
}


//todo test function
template <typename T> PPoints<T> MatUtils::MatToPPoint2(const cv::Mat& m_point){
    //check if mat is empty
    if(m_point.empty()){
        std::__throw_runtime_error("MatUtils::MatToPoint: Input matrix is empty");
    }

    //check if the number of rows is 2
    if(m_point.rows!=2) {
        std::__throw_runtime_error("MatUtils::MatToPoint: Input matrix has to have exactly 2 rows.");
    }

    //copy matrix to points
    std::vector<PPoint<T> > points;
    T x,y;
    for (int n=0;n<m_point.cols;n++){
        //find the datatype of the matrix
        switch(m_point.type()){
            case CV_8UC1:
                x=m_point.at<char>(0,n);
                y=m_point.at<char>(1,n);
                break;
            case CV_32SC1:
                x=m_point.at<int>(0,n);
                y=m_point.at<int>(1,n);
            case CV_32FC1:
                x=m_point.at<float>(0,n);
                y=m_point.at<float>(1,n);
                break;
            case CV_64FC1:
                x=m_point.at<double>(0,n);
                y=m_point.at<double>(1,n);
                break;
                // and so on
            default:
                std::__throw_runtime_error("MatUtils::MatToPPoint3: Type of input mat not supported");
        }
        //check the type


        points.push_back(PPoint<T> (x, y));
    }
    return points;
}

void MatUtils::convertMatToHomogeneousMat(const cv::Mat trans_mat, cv::Mat& output){
    if(trans_mat.cols!=3 && trans_mat.cols!=4){
        CV_Error(1,"MatUtils::convertMatToHomogeneousMat: Mat needs to have 3 or 4 columns");
    }
    Mat h_trans_mat=trans_mat.clone();
    Mat new_row;
    switch (trans_mat.type()){
        case CV_64F:
            new_row=Mat::zeros(1,trans_mat.cols,CV_64F);
            new_row.at<double>(0,new_row.cols-1)=1;
            break;
        case CV_32F:
            new_row=Mat::zeros(1,trans_mat.cols,CV_32F);
            new_row.at<float>(0,new_row.cols-1)=1;
            break;
        default:
            CV_Error(1,"MatUtils::convertMatToHomogeneousMat: Mat must be either of type CV_32F or CV_64F");
            break;
    }
    vconcat(h_trans_mat,new_row,h_trans_mat);
    output=h_trans_mat;
}

vector<PPoints2d> MatUtils::projectPoints3ToImage(vector<PPoints3d> points, const image_geometry::PinholeCameraModel& cam_model) {
    std::vector<PPoints<double> > points2;
    for (int i = 0; i < points.size(); i++) {
        PPoints<double> ppoints;

        for (int j = 0; j < ((Points3d)points[0]).size(); j++) {
            Point2d p = cam_model.project3dToPixel(points[i][j]);
            PPoint<double> pp(p);
            ppoints.push_back(pp);
        }
        points2.push_back(ppoints);
    }
    return points2;
}

Mat MatUtils::RosMat3x3toHomCvMat(tf::Matrix3x3 ros_mat){
    double mat_array[12];
    ros_mat.getOpenGLSubMatrix(mat_array);
    Mat cv_mat=Mat::zeros(4,4,CV_64FC1);
    //set first row
    cv_mat.at<double>(0,0)= static_cast<double>(mat_array[0]);
    cv_mat.at<double>(0,1)=static_cast<double>(mat_array[1]);
    cv_mat.at<double>(0,2)=static_cast<double>(mat_array[2]);
    //set second row
    cv_mat.at<double>(1,0)=static_cast<double>(mat_array[4]);
    cv_mat.at<double>(1,1)=static_cast<double>(mat_array[5]);
    cv_mat.at<double>(1,2)=static_cast<double>(mat_array[6]);
    //set third row
    cv_mat.at<double>(2,0)=static_cast<double>(mat_array[8]);
    cv_mat.at<double>(2,1)=static_cast<double>(mat_array[9]);
    cv_mat.at<double>(2,2)=static_cast<double>(mat_array[10]);
    //set the 1 for the homogeneous transformation
    cv_mat.at<double>(3,3)=1;
    return cv_mat;

}

Mat MatUtils::RosVector3toHomCvMat(tf::Vector3 ros_vec) {
    Mat cv_mat=Mat::zeros(4,4,CV_64FC1);
    //set last column
    cv_mat.at<double>(0,3)= static_cast<double>(ros_vec.x());
    cv_mat.at<double>(1,3)= static_cast<double>(ros_vec.y());
    cv_mat.at<double>(2,3)= static_cast<double>(ros_vec.z());
    cv_mat.at<double>(3,3)=1.0;
    //set idendity
    cv_mat.at<double>(0,0)=1.0;
    cv_mat.at<double>(1,1)=1.0;
    cv_mat.at<double>(2,2)=1.0;

    return cv_mat;
}

Mat MatUtils::RosTransformtoHomCvMat(tf::Transform ros_tf) {
    Mat ros_mat=RosVector3toHomCvMat(ros_tf.getOrigin());
    Mat transl_mat=RosMat3x3toHomCvMat(ros_tf.getBasis());
    Mat cv_mat=ros_mat*transl_mat;
    return cv_mat;
}

template PPoints<char> MatUtils::MatToPPoint2(const Mat& m_point);
template PPoints<short> MatUtils::MatToPPoint2(const Mat& m_point);
template PPoints<int> MatUtils::MatToPPoint2(const Mat& m_point);
template PPoints<float> MatUtils::MatToPPoint2(const Mat& m_point);
template PPoints<double> MatUtils::MatToPPoint2(const Mat& m_point);

template PPoints3<char> MatUtils::MatToPPoint3(const Mat& m_point);
template PPoints3<short> MatUtils::MatToPPoint3(const Mat& m_point);
template PPoints3<int> MatUtils::MatToPPoint3(const Mat& m_point);
template PPoints3<float> MatUtils::MatToPPoint3(const Mat& m_point);
template PPoints3<double> MatUtils::MatToPPoint3(const Mat& m_point);
