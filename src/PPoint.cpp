//
// Created by tman on 12/1/16.
//

#include "PPoint.h"
#include "MatUtils.h"

using namespace std;
using namespace cv;

template <typename T> PPoint<T>::PPoint():Point_<T>() {

}

template <typename T> PPoint<T>::PPoint(T x, T y):Point_<T>(x, y) {

}

template <typename T> PPoint<T>::PPoint(Point_<T> point):Point_<T>(point.x, point.y) {

}

template <typename T> Mat PPoint<T>::toMat() {
    Mat m;
    int t;
    if(typeid(T)== typeid(int)){
        t=CV_32S;
    }else if(typeid(T)== typeid(float)){
        t=CV_32F;
    }else if(typeid(T)== typeid(double)){
        t=CV_64F;
    }else{
        __throw_runtime_error("ERROR: PPoint<T>::toMat(): Datatype not supported. Use either int, float, or double.");
    }
    m=Mat::zeros(2,1,t);
    m.at<T>(0,0)=this->x;
    m.at<T>(0,1)=this->y;
    return m;
}

template <typename T> PPoint<T>::operator Point_<T>(){
    return Point_<T>(this->x,this->y);
}

//performs homogeneous transformation
template <typename T> PPoint<T> operator * (const Mat& m, PPoint<T> point){
    Mat result=m.clone();
    //check if matrix has the right dimension
    if(m.cols!=3|| m.rows!=2){
        CV_Error(1,"Matrix not of dimentsion 2x3. Pass a (float,double) matrix without the homogeneous elements.");
    }
    if(m.type()!=point.toMat().type()){
        CV_Error(1,"Matrix and Point need to be of same type.");
    }
    //convert point to mat
    Mat mpoint=point.toMat();

    //make homogeneous
    MatUtils::convertPointsToHomogeneous(mpoint,mpoint);
    MatUtils::convertMatToHomogeneousMat(m,result);
    //cout<<"mpoint: "<<"\n"<<mpoint<<"\n"<<endl;
    //cout<<"transformation: "<<"\n"<<result<<"\n"<<endl;
    result=result*mpoint;
    //cout<<"Result of transform: \n"<<result<<"\n"<<endl;
    MatUtils::convertPointsFromHomogeneous(result,result);
    //cout<<"Result points: \n"<<result<<"\n"<<endl;
    PPoints<T> p=MatUtils::MatToPPoint2<T>(result);
    return p[0];
}

template class PPoint<int>;
template class PPoint<float>;
template class PPoint<double>;
template class PPoint<char>;
template class PPoint<short>;

template PPoint<char> operator * (const Mat&, PPoint<char>);
template PPoint<short> operator * (const Mat&, PPoint<short>);
template PPoint<int> operator * (const Mat&, PPoint<int>);
template PPoint<float> operator * (const Mat&, PPoint<float>);
template PPoint<double> operator * (const Mat&, PPoint<double>);