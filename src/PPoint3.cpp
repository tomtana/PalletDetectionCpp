//
// Created by tman on 12/1/16.
//
#include "PPoint3.h"
#include "MatUtils.h"

using namespace std;
using namespace cv;


template <typename T> PPoint3<T>::PPoint3():cv::Point3_<T>() {

}

template <typename T> PPoint3<T>::PPoint3(Point3_<T> point):/*x(point.x),y(point.y),z(point.z)*/
        cv::Point3_<T>(point.x,point.y,point.z)
{

}


template <typename T> PPoint3<T>::PPoint3(T x, T y, T z):cv::Point3_<T>(x, y, z) {

}

template <typename T> cv::Mat PPoint3<T>::toMat() {
    cv::Mat m;
    int t;
    if(typeid(T)== typeid(int)){
        t=CV_32S;
    }else if(typeid(T)== typeid(float)){
        t=CV_32F;
    }else if(typeid(T)== typeid(double)){
        t=CV_64F;
    }else{
        std::__throw_runtime_error("ERROR: PPoint3<T>::toMat(): Datatype not supported. Use either int, float, or double.");
    }
    m=cv::Mat::zeros(3,1,t);
    m.at<T>(0,0)=this->x;
    m.at<T>(0,1)=this->y;
    m.at<T>(0,2)=this->z;
    return m;
}
template <typename T> PPoint3<T>::operator cv::Point3_<T>(){
    return cv::Point3_<T>(this->x,this->y,this->z);
}

//performs homogeneous transformation
template <typename T> PPoint3<T> operator * (const Mat& m, PPoint3<T> point){
    Mat result=m.clone();
    //check if matrix has the right dimension
    if(m.cols!=4|| (m.rows!=2&&m.rows!=3)){
        CV_Error(1,"Matrix not of dimentsion 2x4, 3x4. Pass a (float,double) matrix without the homogeneous elements.");
    }
    if(m.type()!=point.toMat().type()){
        CV_Error(1,"Matrix and Point need to be of same type.");
    }
    //convert point to mat
    Mat mpoint=point.toMat();

    //make homogeneous
    MatUtils::convertPointsToHomogeneous(mpoint,mpoint);
    MatUtils::convertMatToHomogeneousMat(m,result);
    //cout<<"mpoint: "<<"\n"<<mpoint<<"\n"<<endl;
    //cout<<"transformation: "<<"\n"<<result<<"\n"<<endl;
    result=result*mpoint;
    //cout<<"Result of transform: \n"<<result<<"\n"<<endl;
    MatUtils::convertPointsFromHomogeneous(result,result);
    //cout<<"Result points: \n"<<result<<"\n"<<endl;
    PPoints3<T> p=MatUtils::MatToPPoint3<T>(result);
    return p[0];
}


template class PPoint3<int>;
template class PPoint3<float >;
template class PPoint3<double >;
template class PPoint3<char >;
template class PPoint3<short>;

template PPoint3<char> operator * (const Mat&, PPoint3<char>);
template PPoint3<short> operator * (const Mat&, PPoint3<short>);
template PPoint3<int> operator * (const Mat&, PPoint3<int>);
template PPoint3<float> operator * (const Mat&, PPoint3<float>);
template PPoint3<double> operator * (const Mat&, PPoint3<double>);