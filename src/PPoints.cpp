//
// Created by tman on 12/1/16.
//

#include "PPoints.h"

using namespace std;
using namespace cv;

template <typename T> PPoints<T>::PPoints(){
}

template <typename T> PPoints<T>::PPoints(std::vector<PPoint<T> > points){
    points_=points;
}

template <typename T> PPoints<T>::PPoints(std::vector<Point_<T> > points){
    for (int i=0;i<points.size();i++){
        points_.push_back(PPoint<T>(points[i]));
    }
}

template <typename T> std::vector<PPoint<T> >& PPoints<T>::getPoints(){
    return points_;
}

template <typename T> Mat PPoints<T>::toMat() {
    Mat m;
    if(points_.empty()){
        return m;
    }
    m=points_[0].toMat();
    for (int i=1;i<points_.size();i++){
        hconcat(m,points_[i].toMat(),m);
    }
    //cout<<m;
    return m;
}

template <typename T> PPoint<T> & PPoints<T>::operator [](int idx){
    return points_[idx];
}

template <typename T> PPoints<T>& PPoints<T>::operator =(std::vector< Point_<T> > points){
    std::vector<PPoint<T> > ppoints;
    for(int i=0;i<points.size();i++){
        ppoints.push_back(PPoint<T>(points[i]));
    }
    points_=ppoints;
    return *this;
}

template <typename T> PPoints<T>& PPoints<T>::operator =(std::vector< PPoint<T> > points){
    points_=points;
    return *this;
}

template <typename T> void PPoints<T>::push_back(Point_<T> point){
    points_.push_back(PPoint<T>(point));
}
template <typename T> void PPoints<T>::push_back(PPoint<T> point){
    points_.push_back(point);
}


template <typename T> PPoints<T>::operator std::vector<cv::Point_<T> >(){
    std::vector<Point_<T> > points;
    for(int i=0;i<points_.size();i++){
        points.push_back(points_[i]);
    }
    return points;
}

template <typename T> PPoints<T> operator * (const cv::Mat& m, PPoints<T> points){
    PPoints<T> points_transf;
    for(int i=0;i<((vector<Point_<T> >)points).size();i++){
        points_transf.push_back(m*points[i]);
    }
    return points_transf;
}

template class PPoints<int>;
template class PPoints<float >;
template class PPoints<double >;
template class PPoints<char >;
template class PPoints<short>;


//instantiate the common template types
template PPoints<char> operator * (const Mat&, PPoints<char>);
template PPoints<short> operator * (const Mat&, PPoints<short>);
template PPoints<int> operator * (const Mat&, PPoints<int>);
template PPoints<float> operator * (const Mat&, PPoints<float>);
template PPoints<double> operator * (const Mat&, PPoints<double>);