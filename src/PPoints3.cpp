//
// Created by tman on 12/1/16.
//

#include "PPoints3.h"

using namespace std;
using namespace cv;

template <typename T> PPoints3<T>::PPoints3(){
}

template <typename T> PPoints3<T>::PPoints3(std::vector<PPoint3<T> > points){
    points_=points;
}

template <typename T> PPoints3<T>::PPoints3(std::vector<Point3_<T> > points){
    //points_=(vector<PPoint3<T> >)points;
    for(int i=0;i<points.size();i++){
        points_.push_back(PPoint3<T>(points[i]));
    }
}

template <typename T> Mat PPoints3<T>::toMat() {
    Mat m;
    if(points_.empty()){
        return m;
    }
    m=points_[0].toMat();
    for (int i=1;i<points_.size();i++){
        hconcat(m,points_[i].toMat(),m);
    }
    //cout<<m;
    return m;
}

template <typename T>  PPoints3<T>::operator std::vector<Point3_<T> >(){
    std::vector<Point3_<T> > points;
    for(int i=0;i<points_.size();i++){
        points.push_back(points_[i]);
    }
    return points;
}

template <typename T> void PPoints3<T>::push_back(PPoint3<T> point){
    points_.push_back(point);
}

template <typename T> PPoint3<T>& PPoints3<T>::operator [](int idx){
    return points_[idx];
}

template <typename T> PPoints3<T> operator * (const cv::Mat& m, PPoints3<T> points){
    PPoints3<T> points_transf;
    for(int i=0;i<((vector<Point3_<T> >)points).size();i++){
        points_transf.push_back(m*points[i]);
    }
    return points_transf;
}



template class PPoints3<int>;
template class PPoints3<float >;
template class PPoints3<double >;
template class PPoints3<char >;
template class PPoints3<short>;

//instantiate the common template types
template PPoints3<char> operator * (const Mat&, PPoints3<char>);
template PPoints3<short> operator * (const Mat&, PPoints3<short>);
template PPoints3<int> operator * (const Mat&, PPoints3<int>);
template PPoints3<float> operator * (const Mat&, PPoints3<float>);
template PPoints3<double> operator * (const Mat&, PPoints3<double>);