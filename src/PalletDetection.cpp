#include <PalletDetection.h>
#include <LineUtils.h>

//temporary includes
#include "opencv2/imgproc/imgproc.hpp"
#include "PalletDetectionSettings.h"
#include "CVMorphology.h"
#include "PclOpenCvBridge.h"
#include <pcl/visualization/cloud_viewer.h>

#include "Image/ImageIO.h"
#include "Fitline/LFLineFitter.h"
#include "LMLineMatcher.h"
#include "FastDCM.h"

#include <image_geometry/stereo_camera_model.h>
#include <stereo_image_proc/processor.h>

using namespace std;
using namespace cv;


PalletDetection::PalletDetection(ros::NodeHandle &nh):
        nh_(nh),            //init node handle
        ros_it_(nh_)        //init image transport
{

    //assign standart topics
    //todo use PalletSettings class and ros parameters to set those parameters
    ROS_INFO("Subscribing to camera topics..");
    ros_topic_img_left_=ROS_TOPIC_IMAGE_LEFT;
    ros_topic_cam_info_left_=ROS_TOPIC_CAM_INFO_LEFT;
    ros_topic_img_right_=ROS_TOPIC_IMAGE_RIGHT;
    ros_topic_cam_info_right_=ROS_TOPIC_CAM_INFO_RIGHT;
    //subscribe topic image topics
    ROS_INFO("Subscribing to topics..");
    ros_it_sub_left_=ros_it_.subscribe(ros_topic_img_left_, 1, &PalletDetection::ImageLeftCallback, this);
    ros_it_sub_right_=ros_it_.subscribe(ros_topic_img_right_, 1, &PalletDetection::ImageRightCallback, this);
    //subscribe cameraInfo topics
    ros_sub_cam_info_left_=nh_.subscribe(ros_topic_cam_info_left_,1, &PalletDetection::CameraInfoLeftCallback, this);
    ros_sub_cam_info_right_=nh_.subscribe(ros_topic_cam_info_right_,1, &PalletDetection::CameraInfoRightCallback, this);
    ROS_INFO("Advertising topics of rectified images:");
    ROS_INFO("Topic left image:\t",ROS_TOPIC_IMAGE_RECT_LEFT);
    ROS_INFO("Topic right image:\t%s",ROS_TOPIC_IMAGE_RECT_RIGHT);
    ros_pub_img_undist_left_= ros_it_.advertise(ROS_TOPIC_IMAGE_RECT_LEFT,1);
    ros_pub_img_undist_right_= ros_it_.advertise(ROS_TOPIC_IMAGE_RECT_RIGHT,1);
    //Initial callback check
    ros_recv_cam_info_left_=false;
    ros_recv_cam_info_right_=false;
    ros_recv_img_left_=false;
    ros_recv_img_right_=false;
    ros_recv_pallet_model_to_cam=false;
    ros_recv_marker_pose=false;
    //
    pallet_detection_init=false;
    pallet_model.load(ros::package::getPath("pallet_detection").append("/yaml/pallet_model_front.yaml"));


}

void PalletDetection::ImageLeftCallback(const sensor_msgs::ImageConstPtr& image_msg){
    Mat img=cv_bridge::toCvShare(image_msg)->image;
    cvtColor(img, img, CV_BGR2GRAY);
    //safe raw image
    image_raw_left_.update(img, image_msg->header);


    //safe rectified image
    //--------------------------------------

    //Verify if camera_info was received
    if(ros_recv_cam_info_left_==false){
        return;
    }

    // If zero distortion, just pass the message along
    bool zero_distortion = true;
    const sensor_msgs::CameraInfo & info_msg= ros_cam_model_left_.cameraInfo();
    for (size_t i = 0; i < info_msg.D.size(); ++i)
    {
        if (info_msg.D[i] != 0.0)
        {
            zero_distortion = false;
            break;
        }
    }
    // This will be true if D is empty/zero sized
    if (zero_distortion)
    {
        ROS_WARN("Left Camera zero distortion.");
        image_undist_left_=image_raw_left_;
        return;
    }

    //Interpolation method 0=nearest neighbor. check https://github.com/ros-perception/image_pipeline/blob/indigo/image_proc/cfg/Rectify.cfg
    int interpolation=4;
    // Rectify and publish
    cv::Mat rect;
    ros_cam_model_left_.rectifyImage(image_raw_left_.getCvImage(), rect, interpolation);
    image_undist_left_.update(rect,image_msg->header);
    ros_recv_img_left_=true;
    //publish rectified image
    // Allocate new rectified image message
    sensor_msgs::ImagePtr rect_msg;
    rect_msg = cv_bridge::CvImage(image_msg->header, sensor_msgs::image_encodings::MONO8, image_undist_left_.getCvImage()).toImageMsg();
    ros_pub_img_undist_left_.publish(rect_msg);
}
void PalletDetection::ImageRightCallback(const sensor_msgs::ImageConstPtr& image_msg){
    //safe raw image
    Mat img=cv_bridge::toCvShare(image_msg)->image;
    cvtColor(img,img,CV_BGR2GRAY);
    image_raw_right_.update(img,image_msg->header);

    //safe rectified image
    //--------------------------------------

    //Verify that camera_info was received
    if(ros_recv_cam_info_right_==false){
        return;
    }

    // If zero distortion, just pass the message along
    bool zero_distortion = true;
    const sensor_msgs::CameraInfo & info_msg= ros_cam_model_right_.cameraInfo();
    for (size_t i = 0; i < info_msg.D.size(); ++i)
    {
        if (info_msg.D[i] != 0.0)
        {
            zero_distortion = false;
            break;
        }
    }
    // This will be true if D is empty/zero sized
    if (zero_distortion)
    {
        ROS_WARN("Left Camera zero distortion.");
        image_undist_right_=image_raw_right_;
        return;
    }

    //Interpolation method 0=nearest neighbor. check https://github.com/ros-perception/image_pipeline/blob/indigo/image_proc/cfg/Rectify.cfg
    int interpolation=4;
    // Rectify and publish
    cv::Mat rect;
    ros_cam_model_right_.rectifyImage(image_raw_right_.getCvImage(), rect, interpolation);
    image_undist_right_.update(rect,image_msg->header);
    ros_recv_img_right_=true;
    //publish rectified image
    // Allocate new rectified image message
    sensor_msgs::ImagePtr rect_msg;
    rect_msg = cv_bridge::CvImage(image_msg->header, sensor_msgs::image_encodings::MONO8, image_undist_right_.getCvImage()).toImageMsg();
    ros_pub_img_undist_right_.publish(rect_msg);
}

//receive camera_info message and unsubscribe the topic if successful
void PalletDetection::CameraInfoLeftCallback(const sensor_msgs::CameraInfoConstPtr & info_msg){
    // Verify camera is actually calibrated
    if (info_msg->K[0] == 0.0) {
        ROS_INFO_ONCE("PalletDetection::CameraInfoLeftCallback: Left camera is uncalibrated.");
        return;
    }

    // Update the camera model
    ros_cam_model_left_.fromCameraInfo(info_msg);
    ros_recv_cam_info_left_=true;
    ROS_INFO("PalletDetection::CameraInfoLeftCallback: Left camera calibration received. Shutting down subscription..");
    //unsubscribe image image topic
    ros_sub_cam_info_left_.shutdown();

}

//receive camera_info message and unsubscribe the topic if successful
void PalletDetection::CameraInfoRightCallback(const sensor_msgs::CameraInfoConstPtr & info_msg){
    // Verify camera is actually calibrated
    if (info_msg->K[0] == 0.0) {
        ROS_INFO_ONCE("Right camera is uncalibrated.");
        return;
    }

    // Update the camera model
    ros_cam_model_right_.fromCameraInfo(info_msg);
    ros_recv_cam_info_right_=true;
    //unsubscribe image image topic
    ROS_INFO("Right camera calibration received. Shutting down subscription..");
    ros_sub_cam_info_right_.shutdown();
}
void PalletDetection::MainLoop(){
    if(!ros_recv_pallet_model_to_cam){
        ROS_INFO_ONCE("Waiting for pallet_model to cam_left transform.");
        GetTfPalletModel2Cam();
        return;
    }
    if(!GetMarkerPosition()){
        ROS_INFO_ONCE("Waiting for MarkerPose.");
        return;
    }
    if(!ros_recv_cam_info_left_||!ros_recv_cam_info_right_){
        ROS_INFO_ONCE("Waiting for camera_calibration.");
        return;
    }
    if(!ros_recv_img_left_||!ros_recv_img_right_){
        ROS_INFO_ONCE("Waiting for left and right input images.");
        return;
    }
    if(!pallet_detection_init){
        init();
    }

    /**
     * Initialize pallet model according to the marker position
     */

    //update the pallet model
    pallet_model.update(cv_tf_pallet_model_to_cam_left.rowRange(0,3),marker_pose.getCvMatMarkerPose().rowRange(0,3));
    Mat dst,img_edge;

    //create roi
    Rect rect_img_roi=PalletDetectionSettings::getPalletRoi(ros_cam_model_left_,marker_pose.getCvMatMarkerPose());
    Mat img_left_roi=image_undist_left_.getCvImage()(rect_img_roi);


    //compute the angle
    pallet_model.getAngles();


    cout<<"-------Detection attempt---------:\n@"<<img_left_roi.rows<<" x "<<img_left_roi.cols<<endl;
    /**
     * Preprocessing CLAHE and Bilateral filter to preserve edges
     */
    Mat img_left_bi;
    Mat img_left_roi_small;
    double scale=1;
    int img_size=256;
    //resize image
    if(img_left_roi.cols>img_size){
        scale = static_cast<double>(img_size)/img_left_roi.cols;
        Size img_left_roi_size(img_left_roi.cols*scale,img_left_roi.rows*scale);
        cout<<"Scale:  "<<scale<<endl;
        cout<<"New img size:  "<<img_left_roi_size<<endl;
        resize(img_left_roi,img_left_roi_small,img_left_roi_size,0,0,CV_INTER_AREA);
    }

    //remove noise whils t maintaining the edges with bilateral filter
    int64 e1 = cv::getTickCount();
    bilateralFilter ( img_left_roi_small, img_left_bi, 3, 3, 3 );
    int64 e2 = cv::getTickCount();
    double time = (e2 - e1)/ cv::getTickFrequency();
    cout<<"Time: Bilateral filter:\t \t \t"<<time<<endl;
    //imshow("Old Scale",img_left_bi);
    //imshow("New Scale",img_left_roi);
    //waitKey();

    //make contrast enhancement with Histogramm equalization
    e1 = cv::getTickCount();
    Ptr<CLAHE> clahe = createCLAHE();
    clahe->setClipLimit(2);
    clahe->setTilesGridSize(Size(17,17));
    clahe->apply(img_left_bi,img_left_bi);
    img_left_roi=img_left_bi;
    e2 = cv::getTickCount();
    time = (e2 - e1)/ cv::getTickFrequency();
    cout<<"Time: CLAHE filter:\t \t \t"<<time<<endl;
    //equalizeHist( img_left_roi, img_left_roi );


    /**
     * Canny edge image
     */
    e1 = cv::getTickCount();
    //find threshold for edge detection
    Mat test;
    double otsu_thresh_val = cv::threshold(
            img_left_roi,test , 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU
    );
    double high_thresh_val  = otsu_thresh_val,
            lower_thresh_val = otsu_thresh_val * 0.1;
    cv::Canny( img_left_roi, img_edge, lower_thresh_val, high_thresh_val );
    e2 = cv::getTickCount();
    time = (e2 - e1)/ cv::getTickFrequency();
    cout<<"Time: Canny+OTSU:\t \t \t"<<time<<endl;
    dst=img_edge.clone();
    //imshow("origial image edge",dst);
    //imshow("origial image",img_left_roi);


    /**
     * Morphological filter
     */
    //compute erosion with opencv
    e1 = cv::getTickCount();
    Mat img_erode1,img_erode2;
    erode(dst,img_erode1,CVMorphology::getLine(90,3.0),Point(-1,-1),2);
    erode(dst,img_erode2,CVMorphology::getLine((pallet_model.getAngles())[0]*180/CV_PI,9.0),Point(-1,-1),1);
    dst=(img_erode1|img_erode2);
    dst=CVMorphology::reconstructByDilation(dst,img_edge, 5,false);
    e2 = cv::getTickCount();
    time = (e2 - e1)/ cv::getTickFrequency();
    cout<<"Time: Morphological filter:\t \t"<<time<<endl;


    /**
     * Creating Mats for visualisation of Results
     */
    Mat chamfer_result, chamfer_result_edge;
    cvtColor(dst,chamfer_result_edge,CV_GRAY2BGR);
    cvtColor(img_left_roi,chamfer_result,CV_GRAY2BGR);

    /**
     * Fast Directional Chamfer Matching
     */

    vector< vector<LMDetWind> > detWindArrays;
    vector<LMDetWind> detWindArray;
    e1 = cv::getTickCount();
    FastDCM fdcm;
    vector<Mat> templ_vec;
    templ_vec.push_back(pallet_model.draw(1,2,2,scale));
    fdcm.performDetection(dst,templ_vec,0.4,detWindArrays);
    e2 = cv::getTickCount();
    time = (e2 - e1)/ cv::getTickFrequency();
    cout<<"Time: FDCM:        \t \t \t"<<time<<endl;

    if(detWindArrays.size()>0){
        if(detWindArrays[0].size()>0){
            cout<<"FDCM Result:"<<endl;
            for (int j = 0; j < 1; ++j) {
                cout<<detWindArrays[0][j]<<endl;
            }
        }
    }
    if(detWindArray.size()>0){
        cout<<"FDCM Result:"<<endl;
        for (int j = 0; j < 1; ++j) {
            cout<<detWindArray[j]<<endl;
        }
    }


    /**
     * Visualisation of the marker position
     */

    //create 3d marker point
    cv::Point3d marker_p3d(marker_pose.getCvMatMarkerPose().at<double>(0,3), marker_pose.getCvMatMarkerPose().at<double>(1,3),marker_pose.getCvMatMarkerPose().at<double>(2,3)) ;
    cv::Point2i marker_p2i=ros_cam_model_left_.project3dToPixel(marker_p3d);
    //consider offset from roi for marker position
    cv::Point2i roi_offset(rect_img_roi.x,rect_img_roi.y);
    marker_p2i=(marker_p2i-roi_offset)*scale;


    /**
     * Visualisation of FDCM results
     */
    if(detWindArrays.size()>0){
        if(detWindArrays[0].size()>0){
            rectangle(chamfer_result,cv::Point(detWindArrays[0][0].x_,detWindArrays[0][0].y_),cv::Point(detWindArrays[0][0].x_+detWindArrays[0][0].width_,detWindArrays[0][0].y_+detWindArrays[0][0].height_),Scalar(0,0,255));
            //Marker position as circle
            circle(chamfer_result,marker_p2i,10,Scalar(255,50,0),10 );

            //visualize on edge image
            rectangle(chamfer_result_edge,cv::Point(detWindArrays[0][0].x_,detWindArrays[0][0].y_),cv::Point(detWindArrays[0][0].x_+detWindArrays[0][0].width_,detWindArrays[0][0].y_+detWindArrays[0][0].height_),Scalar(0,0,255));
            circle(chamfer_result_edge,marker_p2i,10,Scalar(255,50,0),10 );

            imshow("FDCM Result", chamfer_result);
            imshow("FDCM Result edge", chamfer_result_edge);
        }

    }
    else{
        ROS_INFO("FDCM Failed!");
    }

    /**
     * Stereo Matching in opencv
     */
    image_geometry::StereoCameraModel stereo_cam_model;
    stereo_cam_model.fromCameraInfo(ros_cam_model_left_.cameraInfo(),ros_cam_model_right_.cameraInfo());

    //creating the BM algorithm
    Ptr<StereoBM> bm = StereoBM::create(16,9);
    Ptr<StereoSGBM> sgbm = StereoSGBM::create(0,16,3);
    //init config
    Size image_size_left = image_raw_left_.getCvImage().size();

    Rect roi1, roi2;
    Mat Q;

    Mat M1, D1, M2, D2;
    Mat R1, P1, R2, P2;


    M1=Mat(ros_cam_model_left_.fullIntrinsicMatrix());
    D1=ros_cam_model_left_.distortionCoeffs();
    R1=Mat(ros_cam_model_left_.rotationMatrix());
    P1=Mat(ros_cam_model_left_.projectionMatrix());
    M2=Mat(ros_cam_model_right_.fullIntrinsicMatrix());
    D2=ros_cam_model_right_.distortionCoeffs();
    R2=Mat(ros_cam_model_right_.rotationMatrix());
    P2=Mat(ros_cam_model_right_.projectionMatrix());
    Mat R2old=R2.clone();
    Mat T;
    Mat R1t,R2t;
    transpose(R1,R1t);
    transpose(R2,R2t);
    P2.col(3).copyTo(T);

    //stereoRectify( M1, D1, M2, D2, image_size_left, 2*R1t, T, R1, R2, P1, P2, Q, CALIB_ZERO_DISPARITY, -1, image_size_left, &roi1, &roi2 );



    Mat map11, map12, map21, map22;
    initUndistortRectifyMap(M1, D2 , R1, P1, image_size_left, CV_16SC2, map11, map12);
    initUndistortRectifyMap(M2, D2, R2, P2, image_size_left, CV_16SC2, map21, map22);

    Mat img1r, img2r;
    remap(image_raw_left_.getCvImage(), img1r, map11, map12, INTER_LINEAR);
    remap(image_raw_right_.getCvImage(), img2r, map21, map22, INTER_LINEAR);


    //only perform stereo matching over small ROI
    double b=stereo_cam_model.baseline();
    double disp_min=stereo_cam_model.getDisparity(marker_p3d.z+2); //minimum disparity for the block matching algorithm
    double disp_max=stereo_cam_model.getDisparity(marker_p3d.z-1); //this value is used to correct the roi for the block matching such that the box is completely within
    cout<<"Disparity min:\t"<<disp_min<<endl;
    cout<<"Baseline     :\t"<<b<<endl;
    cout<<"Z     :\t"<<marker_p3d.z<<endl;

    //int numberOfDisparities = ((image_size_left.width/8) + 15) & -16;
    int numberOfDisparities = 16*ceil(disp_max/16);

    bm->setROI1(roi1);
    bm->setROI2(roi2);
    bm->setPreFilterCap(63);
    bm->setBlockSize( 5); //must be a positive odd number
    bm->setMinDisparity(static_cast<int>(disp_min));
    bm->setMinDisparity(0);
    bm->setNumDisparities(numberOfDisparities);
    bm->setTextureThreshold(15);
    bm->setUniquenessRatio(10);
    bm->setSpeckleWindowSize(100);
    bm->setSpeckleRange(32);
    bm->setDisp12MaxDiff(1);

    sgbm->setPreFilterCap(63);
    int sgbmWinSize = 3;
    sgbm->setBlockSize(sgbmWinSize);

    int cn = img1r.channels();

    sgbm->setP1(8*cn*sgbmWinSize*sgbmWinSize);
    sgbm->setP2(32*cn*sgbmWinSize*sgbmWinSize);
    sgbm->setMinDisparity(static_cast<int>(disp_min));
    sgbm->setMinDisparity(0);
    sgbm->setNumDisparities(numberOfDisparities);
    sgbm->setUniquenessRatio(10);
    sgbm->setSpeckleWindowSize(100);
    sgbm->setSpeckleRange(32);
    sgbm->setDisp12MaxDiff(1);
    int alg=cv::StereoSGBM::MODE_SGBM;
    if(alg==cv::StereoSGBM::MODE_HH)
        sgbm->setMode(StereoSGBM::MODE_HH);
    else if(alg==cv::StereoSGBM::MODE_SGBM)
        sgbm->setMode(StereoSGBM::MODE_SGBM);
    else if(alg==cv::StereoSGBM::MODE_SGBM_3WAY)
        sgbm->setMode(StereoSGBM::MODE_SGBM_3WAY);

    Mat disp, disp8, dispsbm,disp8sbm;
    //Mat img1p, img2p, dispp;
    //copyMakeBorder(img1, img1p, 0, 0, numberOfDisparities, 0, IPL_BORDER_REPLICATE);
    //copyMakeBorder(img2, img2p, 0, 0, numberOfDisparities, 0, IPL_BORDER_REPLICATE);

    Rect rect_disp_roi=rect_img_roi;
    rect_disp_roi.x=max(rect_disp_roi.x- static_cast<int>(disp_max),0);
    rect_disp_roi.width=min(rect_disp_roi.width+ static_cast<int>(disp_max),img1r.cols);

    img1r(rect_disp_roi).copyTo(img1r);
    img2r(rect_disp_roi).copyTo(img2r);
    //comppute disparity
    int64 t = getTickCount();
    //bm->compute(img1r, img2r, disp);
    sgbm->compute(img1r, img2r, dispsbm);
    t = getTickCount() - t;
    printf("Time disparity:\t\t %fms\n", t*1000/getTickFrequency());
    //disp.convertTo(disp8, CV_8U, 255/(numberOfDisparities*16.));
    dispsbm.convertTo(disp8sbm, CV_8U, 255/(numberOfDisparities*16.));
    //namedWindow("disparity", 0);
    //imshow("disparity", disp8);
    namedWindow("disparity sbm", 0);
    imshow("disparity sbm", disp8sbm);
    //imshow("Left",img1r);
    //imshow("right",img2r);
    Mat pcl_cv;
    stereo_cam_model.projectDisparityImageTo3d(dispsbm,pcl_cv);

    //convert to pcl
    Mat img1r32f;
    img1r.convertTo(img1r32f,CV_32F);
    cv::normalize(img1r32f,img1r32f,0,255,NORM_MINMAX);
    pcl::PointCloud<pcl::PointXYZI>::Ptr pcl_pcl= PclOpenCvBridge::MatCopyToPointXYZI(pcl_cv,img1r32f);

    pcl_viewer->setBackgroundColor (0, 0, 0);

    pcl::visualization::PointCloudColorHandlerGenericField<pcl::PointXYZI> intensity(pcl_pcl,"intensity");
    pcl_viewer->removeAllPointClouds();
    pcl_viewer->removeCoordinateSystem("origin");
    pcl_viewer->addPointCloud<pcl::PointXYZI> (pcl_pcl, intensity, "pallet cloud");
    pcl_viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 2, "pallet cloud");
    pcl_viewer->addCoordinateSystem(0.05,0.0,0.0,0.0,"origin",0);
    pcl_viewer->spinOnce(1,true);

    cout<<"-------------END------------\n\n"<<endl;

    waitKey(10);


    //todo check here if marker was already received


    //reset the image recv to false
    ros_recv_img_left_=false;
    ros_recv_img_right_=false;
}

void PalletDetection::GetTfPalletModel2Cam() {
    tf::StampedTransform transform;
    //check if frame existes
    if(!ros_mp_listener.frameExists("/pallet_model")){
        ROS_ERROR_ONCE("No such TF frame exists: %s", "/pallet_model");
        return;
    }
    if(!ros_mp_listener.frameExists("/left")){
        ROS_ERROR_ONCE("No such TF frame exists: %s", "/left");
        return;
    }
    try{
        //
        //ros_mp_listener.waitForTransform("/left","/Patt.ID=0",ros::Time::now(),ros::Duration(0.02));
        ros_mp_listener.lookupTransform("/left","/pallet_model",ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
        ROS_ERROR("%s",ex.what());
        //ros::Duration(1.0).sleep();
        return;
    }

    cv_tf_pallet_model_to_cam_left=MatUtils::RosTransformtoHomCvMat(transform);
    ROS_INFO("Transformation pallet_model to camera_left successfully load");
    //cout<<"pallet_model to cam transform:"<<cv_tf_pallet_model_to_cam_left<<endl;
    ros_recv_pallet_model_to_cam=true;
}

//todo this function is only a test. For production it has to be adapter to handle multiple detected frames from pallets
bool PalletDetection::GetMarkerPosition() {
    tf::StampedTransform transform;
    //check if frame existes
    if(!ros_mp_listener.frameExists("/Patt.ID=0")){
        ROS_ERROR_ONCE("No such TF frame exists: %s", "/Patt.ID=0");
        return false;
    }
    try{
        //
        //ros_mp_listener.waitForTransform("/left","/Patt.ID=0",ros::Time::now(),ros::Duration(0.02));
        ros_mp_listener.lookupTransform("/left","/Patt.ID=0",ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
        ROS_ERROR("%s",ex.what());
        //ros::Duration(1.0).sleep();
        return false;
    }
    //todo check that the transform doesnt exceed a time limit
    //todo keep track of the time stamps in order to be able to know what tf i use to detect the pallet
    //check if the marker has a valid position with respect to the camera
    if(transform.getOrigin().z()<0){
        return false;
    }
    //only consider the rotation about the y axis
    double roll, pitch, yaw;
    // returns the roll pitch and yaw over the xyz axis respectively
    transform.getBasis().getRPY(roll, pitch, yaw);
    tf::Matrix3x3 new_rot;
    //here we set the new rotation which will be used to transform the pallet model to the marker position
    //be aware that only the rotation around the y axis is taken into account
    //the pitch value is inverted because of the marker frame
    new_rot.setRPY(0,-pitch,0);
    transform.setBasis(new_rot);
    marker_pose.update(transform);
    //cout<<"Marker Transform\n"<<marker_pose.getCvMatMarkerPose()<<endl;
    //ROS_INFO("New Transform received");

    ros_recv_marker_pose=true;

    return true;
}



void PalletDetection::init(){
    //init pallet model
    pallet_model.setCameraModel(ros_cam_model_left_);
    pallet_detection_init=true;

    //pcl visualizer init
    pcl_viewer.reset(new pcl::visualization::PCLVisualizer ("Pallet Viewer"));
    pcl_viewer->setBackgroundColor (0, 0, 0);
    pcl_viewer->setCameraPosition(-0.0506436, 0.0184764, 0.0514701,-0.109344, 0.258179, -0.9176,0.0228493, 0.970812, 0.238749);
    pcl_viewer->setCameraClipDistances(0.00522511, 8.22511);
    pcl_viewer->setSize(942, 774);
    Eigen::Matrix3f intrinsic;
    intrinsic<< ros_cam_model_left_.fx(),0,ros_cam_model_left_.cx(),
           0,ros_cam_model_left_.fy(),ros_cam_model_left_.cy(),
           0,0,1;
    Eigen::Matrix4f extrinsic;
    extrinsic<<1.0, 0.0,    0.0,    0.0,
               0.0, -1.0,   0.0,    0.0,
              0,   0,      -1,     0,
              0,   0,      0,      1;

    //pcl_viewer->setCameraParameters(intrinsic,extrinsic);
    ROS_INFO("PalletDetection has been initialized.");
}
