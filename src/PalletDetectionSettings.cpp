//
// Created by tman on 16/11/16.
//

#include "PalletDetectionSettings.h"
#include "PalletModel.h"
#include "MatUtils.h"
#include "LineUtils.h"
using namespace cv;

PalletDetectionSettings::PalletDetectionSettings() {

}

//todo auto detect the pallet dimensions
cv::Rect PalletDetectionSettings::getPalletRoi(image_geometry::PinholeCameraModel camera_model, cv::Mat marker_pose) {
    //get 3d points to transform
    Matx44d top_left((double*)marker_pose.clone().ptr());
    Matx44d top_right((double*)marker_pose.clone().ptr());
    Matx44d bottom_center((double*)marker_pose.clone().ptr());
    //get projection matrix
    Matx34d proj_mat=camera_model.fullProjectionMatrix();
    //add thresholds of the pallet
    top_left(0,3)=top_left(0,3)-1.2;
    top_right(0,3)=top_right(0,3)+1.2;
    bottom_center(1,3)=bottom_center(1,3)+1;

    //cout<<top_left<<endl;
    //cout<<top_right<<endl;
    //cout<<bottom_center<<endl;
    //project points
    Mat m_top_left=Mat(proj_mat)*Mat(top_left).col(3).clone();
    Mat m_top_right=Mat(proj_mat)*Mat(top_right).col(3).clone();
    Mat m_bottom_center=Mat(proj_mat)*Mat(bottom_center).col(3).clone();
    //put all points in one matrix
    Mat points2d(m_top_left);
    //cout<<"points2d \n"<<points2d<<endl;
    hconcat(points2d,Mat(m_top_right),points2d);
    hconcat(points2d,Mat(m_bottom_center),points2d);
    //convert to points from homogeneous to cartesian
    MatUtils::convertPointsFromHomogeneous(points2d,points2d);
    std::vector<cv::Point2f> ppoints2f=(MatUtils::MatToPPoint2<float>(points2d));
    //cout<<"points2d \n"<<points2d<<endl;
    //create rectangle.
    Rect rect=cv::boundingRect(ppoints2f);
    //set the rectangle in such a way that the points lie immediately on the bounding line of it
    if (rect.height > 0) {
        rect.height = rect.height - 1;
    } else if (rect.height < 0) {
        //rect.height=rect.height+1;
        CV_Error(1, "LineUtils::BoundingRect: Height of rectangle is negative");
    }
    if (rect.width > 0) {
        rect.width = rect.width - 1;
    } else if (rect.width < 0) {
        //rect.width=rect.width+1;
        CV_Error(1, "LineUtils::BoundingRect: Width of rectangle is negative");
    }
    cv::Size img_size = camera_model.fullResolution();



    //check if rectangle lies within the image boarders
    cv::Vec4f limit_vec=LineUtils::RectToVec(rect);

    if(limit_vec(0)<0){
        limit_vec(0)=0;
    }
    if(limit_vec(1)<0){
        limit_vec(1)=0;
    }
    if(limit_vec(2)>img_size.width){
        limit_vec(2)=img_size.width;
    }
    if(limit_vec(3)>img_size.height){
        limit_vec(3)=img_size.height;
    }


    rect=cv::Rect(limit_vec(0),limit_vec(1),limit_vec(2)-limit_vec(0),limit_vec(3)-limit_vec(1));
    if(rect.height % 2 !=0){
        rect.height=rect.height-1  ;
    }
    if(rect.width % 2 !=0){
        rect.width=rect.width-1  ;
    }
    //cout<<rect<<endl;
    //add to the marker location the threshold
    //m meter maximum height of the marker todo make transformation with respect to the world frame and not to the cam frame!
    //convert mat to point
    //todo extract pallet width from model


    return rect;
}
