//
// Created by tman on 1/21/17.
//

#include "PalletImage.h"
PalletImage::PalletImage() {

}

PalletImage::PalletImage(cv::Mat image, std_msgs::Header header) {
    image_=image.clone();
    header_=header;
}

std_msgs::Header PalletImage::getHeader() {
    return header_;
}

cv::Mat &PalletImage::getCvImage() {
    return image_;
}

cv::Mat PalletImage::getCvImageRoi(cv::Rect roi) {
    cv::Mat roi_mat=image_(roi);
    return roi_mat;
}

void PalletImage::update(cv::Mat image, std_msgs::Header header) {
    image_=image.clone();
    header_=header;
}

PalletImage &PalletImage::operator=(PalletImage &image) {
    image_=image.getCvImage().clone();
    header_=image.getHeader();
    return *this;
}
