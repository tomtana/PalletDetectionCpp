//
// Created by tman on 1/21/17.
//

#include "PalletMarker.h"
#include "MatUtils.h"

PalletMarker::PalletMarker() {

    //init marker pose at the following point--> todo maybe later replaced as variable..
    marker_pose_=cv::Mat(cv::Matx44d(1,0,0,0, 0,1,0,0, 0,0,1,2, 0,0,0,1));
}

PalletMarker::PalletMarker(tf::StampedTransform tf) {
    stamp_=tf.stamp_;
    marker_pose_=MatUtils::RosTransformtoHomCvMat(tf);
}

void PalletMarker::update(tf::StampedTransform tf){
    stamp_=tf.stamp_;
    marker_pose_=MatUtils::RosTransformtoHomCvMat(tf);
}

cv::Mat& PalletMarker::getCvMatMarkerPose(){
    return marker_pose_;
}

ros::Time PalletMarker::getRosTime() {
    return stamp_;
}

cv::Point3d PalletMarker::getTranslation() {
    cv::Point3d transl(marker_pose_.at<double>(0,3),
                       marker_pose_.at<double>(1,3),
                       marker_pose_.at<double>(2,3));

    return transl;
}

cv::Point2i PalletMarker::getImagePoint(image_geometry::PinholeCameraModel cam_model) {
    return cam_model.project3dToPixel(getTranslation());
}
