//
// Created by tman on 29/11/16.
//

#include <MatUtils.h>
#include "PalletModel.h"
#include <yaml-cpp/yaml.h>
#include "LineUtils.h"

using namespace cv;
using namespace std;

PalletModel::PalletModel() {
    /*Matx34f mat = Matx34d(1, 0, 0, 0,
                                0, 1, 0, 0,
                                0, 0, 1, 0);
    tf_cam_to_pallet_=Mat(mat);*/
}

PalletModel::PalletModel(const image_geometry::PinholeCameraModel &cam_model) {
    cam_model_ = cam_model;
}

bool PalletModel::load(string path) {
    //load the file
    YAML::Node baseNode = YAML::LoadFile(path);
    if (baseNode.IsNull()) {
        return false; //File Not Found?
    }

    //container to store the points
    vector<PPoints3<double> > all_points;
    //the points are defined in mm, so we need to convert them to meters.
    float mm_to_m=1000;
    for (YAML::const_iterator it = baseNode.begin(); it != baseNode.end(); ++it) {
        YAML::Node key = it->first;
        if (key.Type() == YAML::NodeType::Scalar) {
            string name = key.as<std::string>();

            //for each key in the file perform parsing
            YAML::Node pg_node = baseNode[name];
            if (pg_node.IsNull()) {
                return false; //name not found?
            }
            //load the points
            std::vector<std::vector<double> > point_vec = pg_node["points"].as<std::vector<std::vector<double> > >();
            if (point_vec.empty()) {
                return false;
            }
            const int num_points = point_vec.size();
            const int num_point_elements = point_vec[0].size();

            if (num_point_elements != 3) {
                return false; //yaml file holds not correct points
            }

            //create PPoints container to store one point group
            PPoints3<double> points;

            for (int i = 0; i < num_points; i++) {
                PPoint3<double> point;
                point.x = point_vec[i][0]/mm_to_m;
                point.y = point_vec[i][1]/mm_to_m;
                point.z = point_vec[i][2]/mm_to_m;
                points.points_.push_back(point);
            }
            all_points.push_back(points);
        }
    }
    //store points to member variable
    points3_ = all_points;
    centerModel3();
    ROS_INFO("Corner points of pallet model successfully loaded from yaml file.");
    //cout<<"Points3\n"<<points3_[0].toMat()<<endl;
    //cout<<"Points3\n"<<points3_[1].toMat()<<endl;
}

void PalletModel::centerModel3() {
    //place the center of the object in the center of the camera
    //1. find min max of x and y
    double max_val_x;
    double min_val_x;
    double max_val_y;
    double min_val_y;

    Mat mp3=getMatPoints3();
    minMaxIdx(mp3.row(0).clone(), &min_val_x, &max_val_x);
    minMaxIdx(mp3.row(1).clone(), &min_val_y, &max_val_y);

    //2. transform points to abs(x_max-x_min)/2 and abs(y_max-y_min)/2
    Matx34d mxt(1,0,0,-(max_val_x-min_val_x)/2,
                0,1,0,-(max_val_y-min_val_y)/2,
                0,0,1,0);
    Mat mt(mxt);
    for (int i = 0; i < points3_.size(); ++i) {
        points3_[i]=mt*points3_[i];
    }
}


//todo finish the distance computation
bool PalletModel::computeDistances() {
    //define ref point
    Mat ref_point;
    int idx[2];
    minMaxIdx(points2_[0].toMat().row(0).clone(), NULL, NULL, idx);
    //cout<<"PalletModel::computeDistances: Idx min x: "<<endl;
    //cout<<idx[1]<<endl;
    points2_[0].toMat().col(idx[1]).copyTo(ref_point);
    //cout<<"PalletModel::computeDistances: New ref point: "<<endl<<ref_point<<endl;

    //compute the distances
    Mat d;
    MatUtils::computePointDistances(points2_[0].toMat(),ref_point,d);
    point_dist_=d;
    //cout<<"PalletModel::computeDistances: Distances:"<<endl;
    //cout<<d<<endl;
}

//todo test
void PalletModel::update(cv::Mat tf_pallet_model_2_cam, cv::Mat tf_cam_to_marker) {
    if (points3_.empty()) {
        CV_Error(1, "points3_ container is empty.");
    }
    if (!cam_model_.initialized()) {
        CV_Error(1, "Camera model has not been initialized yet.");
    }
    if(tf_pallet_model_2_cam.rows!=3 || tf_pallet_model_2_cam.cols!=4){
        CV_Error(1, "tf_pallet_model_2_cam has to be exactly of dimension 3x4. Thus non-homogenous tf matrix");
    }
    if(tf_cam_to_marker.rows!=3 || tf_cam_to_marker.cols!=4){
        CV_Error(1, "tf_cam_to_marker has to be exactly of dimension 3x4. Thus non-homogenous tf matrix");
    }

    vector<PPoints3d> points_transf=transform3DCamToPallet(tf_pallet_model_2_cam,tf_cam_to_marker);
    //cout<<"PalletModel::update: 3d Points"<<endl;
    //cout<<points_transf[0].toMat()<<endl;
    //cout<<points_transf[1].toMat()<<endl;
    points2_=MatUtils::projectPoints3ToImage(points_transf,cam_model_);
    //cout<<"PalletModel::update: 2d Points"<<endl;
    //cout<<points2_[0].toMat()<<endl;
    //cout<<points2_[1].toMat()<<endl;
    computeDistances();
}


void PalletModel::setCameraModel(image_geometry::PinholeCameraModel &cam_model) {
    cam_model_ = cam_model;
}


vector<PPoints3d> PalletModel::transform3DCamToPallet(cv::Mat tf_pallet_model_2_cam, cv::Mat tf_cam_to_marker) {
    vector<PPoints3d> points_transf;
    //ROS_INFO("Now computing the transform");
    for(int i=0;i<points3_.size();i++){
        points_transf.push_back(tf_cam_to_marker*(tf_pallet_model_2_cam*points3_[i]));
    }
    return points_transf;
}

Mat PalletModel::getMatPoints3() {
    Mat m;
    if(points3_.empty()){
        return m;
    }
    m=points3_[0].toMat();
    for (int i = 1; i < points3_.size(); ++i) {
        hconcat(m,points3_[i].toMat(),m);

    }
    return m;
}

cv::Mat PalletModel::draw(int thickness, int x_off, int y_off, double scale) {
    if(scale==0){
        __throw_runtime_error("PalletModel::draw: scale cant not be 0");
    }
    if(scale<0){
        __throw_runtime_error("PalletModel::draw: scale cant not <= 0");
    }
    Points2f points=MatUtils::MatToPPoint2<float>(points2_[0].toMat());
    Points2f points2=MatUtils::MatToPPoint2<float>(points2_[1].toMat());
    for(int i=0;i<points2.size();i++){
        points.push_back(points2[i]);
    }
    Rect rect=cv::boundingRect(points);
    //cout<<rect<<endl;
    Mat pallet_template=Mat::zeros(rect.height+2*x_off,rect.width+2*y_off,CV_8UC1);
    //cout<<pallet_template<<endl;
    //draw horizontal top line
    Point2d offset(rect.x-x_off,rect.y-y_off);
    line(pallet_template,points2_[0].getPoints()[1]-offset,points2_[0].getPoints()[2]-offset,Scalar(255,0,0),thickness,8);
    line(pallet_template,points2_[0].getPoints()[3]-offset,points2_[0].getPoints()[4]-offset,Scalar(255,0,0),thickness,8);
    //draw bottom parts
    line(pallet_template,points2_[1].getPoints()[0]-offset,points2_[1].getPoints()[1]-offset,Scalar(255,0,0),thickness,8);
    line(pallet_template,points2_[1].getPoints()[2]-offset,points2_[1].getPoints()[3]-offset,Scalar(255,0,0),thickness,8);
    line(pallet_template,points2_[1].getPoints()[4]-offset,points2_[1].getPoints()[5]-offset,Scalar(255,0,0),thickness,8);
    //draw vertical lines
    for(int i=0;i<points2_[0].getPoints().size();i++){
        line(pallet_template,points2_[0].getPoints()[i]-offset,points2_[1].getPoints()[i]-offset,Scalar(255,0,0),thickness,8);
    }
    if(scale!=1){
        Size img_left_roi_size(pallet_template.cols*scale,pallet_template.rows*scale);
        //cout<<"Templ Scale:  "<<scale<<endl;
        //cout<<"New Templ Size:  "<<img_left_roi_size<<endl;
        resize(pallet_template,pallet_template,img_left_roi_size,0,0,CV_INTER_AREA);
    }
    return pallet_template;
}
