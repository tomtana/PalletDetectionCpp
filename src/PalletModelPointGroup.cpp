//
// Created by tman on 03/11/16.
//

#include <PalletModelPointGroup.h>
#include <iostream>

PalletModelPointGroup::PalletModelPointGroup() {
}

PalletModelPointGroup::PalletModelPointGroup(string name) {
    name_=name;
}

PalletModelPointGroup::PalletModelPointGroup(Mat points, Mat ref_point,string name) {
    //mxn matrix, containing n elements of point p (mx1)
    point_group_=points.clone();
    ref_point_=ref_point.clone();
    name_=name;
    computeDistances();
}

//computes the distances and chooses the point with the smallest x value as reference
PalletModelPointGroup::PalletModelPointGroup(Mat points,string name) {
    point_group_=points.clone();
    name_=name;
    computeDistances();
}

// Compute all distances between the points in the point_group and the ref point.
// If no ref point is defined, choose the point with the min x value as ref point.
void PalletModelPointGroup::computeDistances() {
    //check if point_group exists
    if (point_group_.empty()){
        return;
    }
    //check if ref_point is already set and set it if not
    if (ref_point_.empty()){
        int idx[2];
        minMaxIdx(point_group_.row(0).clone(), NULL, NULL, idx);
        //cout<<idx[1]<<endl;
        point_group_.col(idx[1]).copyTo(ref_point_);
        //cout<<"New ref point: "<<endl<<ref_point_<<endl;
    };
    //compute the distances
    Mat d;
    MatUtils::computePointDistances(point_group_,ref_point_,d);
    dist_to_ref_point_=d;
    //cout<<d<<endl;
}

void PalletModelPointGroup::getName(string & name) {
    name=name_;
}

void PalletModelPointGroup::setRefPoint(Mat point) {
    ref_point_=point.clone();
    computeDistances();
}

//returns the refrence point
void PalletModelPointGroup::getRefPoint(Mat &point) {
    point=ref_point_.clone();
}

void PalletModelPointGroup::setPointGroup(const Mat points){
    point_group_=points.clone();
    computeDistances();
}

//returns the point group
void PalletModelPointGroup::getPointGroup(Mat &points) {
    points=point_group_.clone();
}

void PalletModelPointGroup::print(){
    cout<<"____________Data Dump____________"<<endl;
    cout<<"Point Group Name: \t"<<name_<<endl;
    cout<<"Points:"<<endl;
    cout<<point_group_<<endl;
    cout<<"Reference point:"<<endl;
    cout<<ref_point_<<endl;
    cout<<"Distances:"<<endl;
    cout<<dist_to_ref_point_<<endl;
}

//parse the ModelPointGroup data from a yaml file
bool PalletModelPointGroup::Load(std::string file, std::string name){
    //load the file
    YAML::Node baseNode = YAML::LoadFile(file);
    if (baseNode.IsNull()){
        return false; //File Not Found?
    }
    //load the target element of the yaml file
    YAML::Node pg_node = baseNode[name];
    if (pg_node.IsNull()){
        return false; //name not found?
    }
    name_=name;
    //load the points
    std::vector< std::vector<float> > point_vec =pg_node["points"].as<std::vector< std::vector<float> > >();
    if (point_vec.empty()){
        return false;
    }
    const int num_points=point_vec.size();
    const int num_point_elements=point_vec[0].size();

    //create mat container for points
    Mat point_mat(num_points,num_point_elements,CV_32F);

    //copy the points from the vector to the mat
    for (int i=0;i<num_points;i++){
        Mat m(point_vec[i]);
        transpose(m,m);
        m.row(0).copyTo(point_mat.row(i));
    }
    //transpose point_mat;
    transpose(point_mat,point_mat);
    point_group_=point_mat;
    computeDistances();
    //cout<<point_mat<<endl;

    return true;
}