//
// Created by tman on 3/21/17.
//

#include <opencv2/core/mat.hpp>
#include <pcl/io/pcd_io.h>
#include <stdlib.h>
#include <iostream>
#include <pcl/point_types.h>
#include "PclOpenCvBridge.h"
#include <pcl/filters/passthrough.h>

using namespace std;

pcl::PointCloud<pcl::PointXYZ>::Ptr PclOpenCvBridge::MatCopyToPointXYZ(cv::Mat cv_pcl)
{
    if(cv_pcl.type()!=CV_32FC3){
        throw std::invalid_argument( "Cv input mat has to be of type CV_32FC3");
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr point_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>( static_cast<int>(cv_pcl.cols),
                                                                                            static_cast<int>(cv_pcl.rows))
                                                                                            );//(new pcl::pointcloud<pcl::pointXYZ>);
    point_cloud_ptr->is_dense=true;
    point_cloud_ptr->sensor_origin_=Eigen::Vector4f(0,0,0,0);
    point_cloud_ptr->sensor_orientation_=Eigen::Quaternionf(0,1,0,0);
    pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_filtered;
    for(int n=0;n<cv_pcl.cols;n++)
    {
        for(int m=0;m<cv_pcl.rows;m++){
            //std::cout<<i<<endl;

            // when color needs to be added:
            //uint32_t rgb = (static_cast<uint32_t>(pr) << 16 | static_cast<uint32_t>(pg) << 8 | static_cast<uint32_t>(pb));
            //point.rgb = *reinterpret_cast<float*>(&rgb);

            pcl::PointXYZ point;
            point.x = cv_pcl.at<cv::Vec3f>(m,n)[0];
            point.y = cv_pcl.at<cv::Vec3f>(m,n)[1];
            point.z = cv_pcl.at<cv::Vec3f>(m,n)[2];
            if(abs(point.x)<15 && abs(point.y)<15 && point.z<15&&point.z>=0){
                point_cloud_ptr -> at(n,m).x=cv_pcl.at<cv::Vec3f>(m,n)[0];
                point_cloud_ptr -> at(n,m).y=cv_pcl.at<cv::Vec3f>(m,n)[1];
                point_cloud_ptr -> at(n,m).z=cv_pcl.at<cv::Vec3f>(m,n)[2];
            }

        }

    }
    return point_cloud_ptr;

}

pcl::PointCloud<pcl::PointXYZI>::Ptr PclOpenCvBridge::MatCopyToPointXYZI(cv::Mat cv_pcl, cv::Mat img_left)
{
    if(cv_pcl.type()!=CV_32FC3){
        throw std::invalid_argument( "Cv input mat has to be of type CV_32FC3");
    }
    if(img_left.type()!=CV_32FC1){
        throw std::invalid_argument( "Cv input img_left has to be of type CV_32FC1");
    }

    pcl::PointCloud<pcl::PointXYZI>::Ptr point_cloud_ptr(new pcl::PointCloud<pcl::PointXYZI>( static_cast<int>(cv_pcl.cols),
                                                                                            static_cast<int>(cv_pcl.rows))  );
    point_cloud_ptr->is_dense=true;
    point_cloud_ptr->sensor_origin_=Eigen::Vector4f(0,0,0,0);
    point_cloud_ptr->sensor_orientation_=Eigen::Quaternionf(0,1,0,0);
    for(int n=0;n<cv_pcl.cols;n++)
    {
        for(int m=0;m<cv_pcl.rows;m++){
            //std::cout<<i<<endl;

            // when color needs to be added:
            //uint32_t rgb = (static_cast<uint32_t>(pr) << 16 | static_cast<uint32_t>(pg) << 8 | static_cast<uint32_t>(pb));
            //point.rgb = *reinterpret_cast<float*>(&rgb);

            pcl::PointXYZI point;
            point.x = cv_pcl.at<cv::Vec3f>(m,n)[0];
            point.y = cv_pcl.at<cv::Vec3f>(m,n)[1];
            point.z = cv_pcl.at<cv::Vec3f>(m,n)[2];
            if(abs(point.x)<15 && abs(point.y)<15 && point.z<15&&point.z>=0){
                point_cloud_ptr -> at(n,m).x=cv_pcl.at<cv::Vec3f>(m,n)[0];
                point_cloud_ptr -> at(n,m).y=cv_pcl.at<cv::Vec3f>(m,n)[1];
                point_cloud_ptr -> at(n,m).z=cv_pcl.at<cv::Vec3f>(m,n)[2];
                point_cloud_ptr -> at(n,m).intensity =img_left.at<float>(m,n);
            }

        }

    }
    return point_cloud_ptr;

}