//
// Created by tman on 29/11/16.
//

#include <limits>
#include "PointModel.h"
#include "LineUtils.h"
#include <opencv2/core/core.hpp>

using namespace std;
using namespace cv;

PointModel::PointModel(){

}
PointModel::PointModel(vector<vector<Point_<double> > > points){
    for(int i=0;i<points.size();i++){
        points2_.push_back(PPoints<double>(points[i]));
    }

}

Mat PointModel::getPoints2() {
    Mat m;
    m=points2_[0].toMat();
    for (int i = 1; i < points2_.size(); ++i) {
        hconcat(m,points2_[i].toMat(),m);

    }
    return m;
}

PointModel::PointModel(vector<PPoints<double> > points){
    points2_=points;
}
bool PointModel::load(string path, string key){


}
bool PointModel::save(string path, string key){


}
bool PointModel::computeDistances(){


}

std::vector<double> PointModel::getAngles() {
    std::vector<double> angle;
    if(!points2_.empty()){

        for(int i=0;i<points2_.size();i++){
            //define line with 2 points and compute the angle
            cv::Vec4f line((points2_[i].getPoints())[0].x,points2_[i].getPoints()[0].y,points2_[i].getPoints().back().x,points2_[i].getPoints().back().y);
            angle.push_back(LineUtils::computeAngle(line));
            double ang_deg=angle[i]*180/CV_PI;
            //cout<<"Angle of elemet i="<<i<<"=  "<<ang_deg<<endl;
        }

    }
    return angle;
}

float PointModel::getDistOf(int idx_1, int idx_2, int at_x) {
    unsigned long idx_max= points2_.size();
    //check if valid indices were passed to the function
    if (idx_1>=idx_max || idx_2>=idx_max){
        CV_Error(1,"ERROR: PointModel::getDistOf: One or both indices are bigger than the number of elements.");
    }
    //create the lines
    Vec4f line1(static_cast<float>(points2_[idx_1].getPoints()[0].x),static_cast<float>(points2_[idx_1].getPoints()[0].y),static_cast<float>(points2_[idx_1].getPoints().back().x),static_cast<float>(points2_[idx_1].getPoints().back().y));
    Vec4f line2(static_cast<float>(points2_[idx_2].getPoints()[0].x),static_cast<float>(points2_[idx_2].getPoints()[0].y),static_cast<float>(points2_[idx_2].getPoints().back().x),static_cast<float>(points2_[idx_2].getPoints().back().y));
    float y_1=LineUtils::getY(line1,at_x);
    float y_2=LineUtils::getY(line2,at_x);
    cout<<"Line1="<<line1<<endl;
    cout<<"Line2="<<line2<<endl;
    cout<<"y_1="<<y_1<<endl;
    cout<<"y_2="<<y_2<<endl;
    return static_cast<float>(round(abs(y_1-y_2)));
}
