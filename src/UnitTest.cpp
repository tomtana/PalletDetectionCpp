//
// Created by tman on 12/1/16.
//


#include <ros/package.h>
#include <LineUtils.h>
#include "UnitTest.h"
#include "PalletModel.h"
using namespace cv;
using namespace std;

UnitTest::UnitTest() {

}

void UnitTest::TestPPoints() {
    cout<<"UnitTest PPoints:"<<endl;
    cout<<"_________________________"<<endl;
    Matx23d m(1,0,4,
              0,1,5);
    Mat mm(m);
    cout<<"Transmat:"<<endl;
    cout<<mm<<endl;
    PPoints2d points;
    points.push_back(PPoint2d(1,1)); //test normal constructor //test push_back functionality
    points.push_back(Point2d(2,2)); //test constructor with cv::Point_<>
    points.push_back(PPoint2d(3,3));
    points.push_back(PPoint2d(4,4));
    points.push_back(PPoint2d(5,5));
    cout<<"Dump Points which are to transform:"<<endl;
    cout<<(Points2d)points<<endl; //test type conversion to cv::Point_<>

    PPoints2d points_transf=mm*points; //test overloaded multiplication operator *
    cout<<"Transformation result"<<endl;
    cout<<(Points2d)points_transf<<endl;
    cout<<"\n\n"<<endl;

}

void UnitTest::TestPPoints3() {
    cout<<"UnitTest PPoints3:"<<endl;
    cout<<"_________________________"<<endl;
    Matx34d m(1.0,0,0,5,
              0.0,1,0,6,
              0,0,1,5);
    Mat mm(m);
    cout<<"Transmat:"<<endl;
    cout<<mm<<endl;
    PPoints3d points;
    points.push_back(PPoint3d(1,1,1)); //test normal constructor //test push_back functionality
    points.push_back(Point3d(2,2,2)); //test constructor with cv::Point_<>
    points.push_back(PPoint3d(3,3,3));
    points.push_back(PPoint3d(4,4,4));
    points.push_back(PPoint3d(5,5,5));
    cout<<"Dump Points which are to transform:"<<endl;
    cout<<(Points3d)points<<endl; //test type conversion to cv::Point_<>

    PPoints3d points_transf=mm*points; //test overloaded multiplication operator *
    cout<<"Transformation result"<<endl;
    cout<<(Points3d)points_transf<<endl;
    cout<<"\n\n"<<endl;
}

void UnitTest::TestPPoint(){
    cout<<"UnitTest PPoint:"<<endl;
    cout<<"_________________________"<<endl;
    //test the homogeneous multiplication
    Matx23f m(1.0,0.0,5,
              0.0,1,6);
    Mat mm(m);
    cout<<"Trans Mat:\n"<<mm<<endl;
    PPoint2f point(1,1);
    cout<<"Point to transform:\n"<<(Point2f)point<<endl;
    point=mm*point;
    cout<<"Transformed Point:\n"<<(Point2f)point<<endl;
    cout<<"\n\n"<<endl;
    //test the conversion between cv::Point and PPoint

}

void UnitTest::TestPPoint3() {
    cout<<"UnitTest PPoint3:"<<endl;
    cout<<"_________________________"<<endl;
    Matx34f m(1.0,0,0,5,
              0.0,1,0,6,
              0,0,1,5);
    Mat mm(m);
    cout<<"Trans Mat:\n"<<mm<<endl;
    PPoint3f point(1,1,1);
    cout<<"Point to transform:\n"<<(Point3f)point<<endl;
    point=mm*point;
    cout<<"Transformed Point:\n"<<(Point3f)point<<endl;
    cout<<"\n\n"<<endl;
    cout<<"\n\n"<<endl;
}
/*
 * //functions have been updated, test not longer functional
void UnitTest::TestPalletModel(image_geometry::PinholeCameraModel& cam_model){
    cout<<"UnitTest PalletModel:"<<endl;
    cout<<"_________________________"<<endl;
    Matx34d m(1.0,0,0,0,
              0.0,1,0,0,
              0,0,1,9);
    Mat mm(m);
    cout<<"Transmat:"<<endl;
    cout<<mm<<endl;
    cout<<"Intrinsic mat:"<<endl;
    cout<<cam_model.intrinsicMatrix()<<endl;
    PalletModel pmodel(cam_model);
    pmodel.load(ros::package::getPath("pallet_detection").append("/yaml/pallet_model_front.yaml"));
    cout<<"UnitTest::TestPalletModel: Compute Distances:"<<endl;
    pmodel.update();
    mm.at<double>(2,3)=5;
    pmodel.update();
    cout<<"\n\n"<<endl;
    cout<<"\n\n"<<endl;
    CV_Error(1,"END TEST");
}
*/
void UnitTest::TestLineUtils(){
    cout<<"UnitTest PalletModel:"<<endl;
    cout<<"_________________________\n"<<endl;

    Vec4d v4d(1.123456789111,2.923456789111,3.523456789111,0.523456789111);
    Vec4i v4i=v4d;
    cout<<v4d<<endl;
    cout<<v4i<<endl;
    //p1 and p2 building a line with rho ==90 -> parallel to x axis
    Point2f p1(0,5);
    Point2f p2(5,5);
    Vec4f line_horiz(p1.x,p1.y,p2.x,p2.y);
    cout<<"Line Horiz: \n"<<line_horiz<<endl;
    //p1 and p2 building a line with rho ==90 -> parallel to x axis
    p1=Point2f(0,10);
    p2=Point2f(5,10);
    Vec4f line_horiz2(p1.x,p1.y,p2.x,p2.y);
    cout<<"Line Horiz2: \n"<<line_horiz2<<endl;
    //p2 and p3 building a line with rho ==0  -> parallel to y axis
    p1=Point2f(2,0);
    p2=Point2f(2,10);
    Vec4f line_vertic(p1.x,p1.y,p2.x,p2.y);
    cout<<"Line Vertic: \n"<<line_vertic<<endl;
    //p2 and p3 building a line with rho ==0  -> parallel to y axis
    p1=Point2f(5,0);
    p2=Point2f(5,10);
    Vec4i line_vertic2(p1.x,p1.y,p2.x,p2.y);
    //now 2 lines not beeing special cases
    p1=Point2f(1,1);
    p2=Point2f(4,4);
    Vec4i line1(p1.x,p1.y,p2.x,p2.y);
    p1=Point2f(1,4);
    p2=Point2f(4,1);
    Vec4f line2(p1.x,p1.y,p2.x,p2.y);

    /*//test inersections
    cout<<"Test intersections\n"<<endl;
    cout<<"____________________________\n"<<endl;
    cout<<"Line Vertic2: \n"<<line_vertic2<<endl;
    cout<<"TestLineUtils: Lines result intersection if lines are parallel:\n"
        <<LineUtils::getIntersection(line_horiz,line_horiz2)<<endl; //test result if lines are parallel<<endl;
    cout<<"TestLineUtils: Lines result intersection if lines are parallel:\n"
        <<LineUtils::getIntersection(line_vertic,line_vertic2)<<endl; //test result if lines are parallel<<endl;
    cout<<"TestLineUtils: Lines result intersection if lines horiz and vertical:\n"
        <<LineUtils::getIntersection(line_horiz2,line_vertic2)<<endl; //test result if lines are hotiz and vertical;
    cout<<"TestLineUtils: Lines result intersection if line1 and line2:\n"
        <<LineUtils::getIntersection(line1,line2)<<endl; //test result if lines are normal lines;*/

    //test getX, getY functions
    Point2f lvertic(1,0); //vertical line
    Point2f lvertic2(50,0); //another vertical line
    Point2f lhoriz(1,(90.0f/180.0f*static_cast<float>(CV_PI))); //horiziontal
    Point2f lhoriz2(15,(90.0f/180.0f* static_cast<float>(CV_PI))); //horiziontal
    Point2f lnormal(1,(15.0f/180.0f*static_cast<float>(CV_PI))); //line
    Point2f lnormal2(500,40.0f/180.0f*static_cast<float>(CV_PI)); //line

    cout<<"getX vertical line:\n"<<LineUtils::getX(lvertic,0)<<endl;
    cout<<"getX vertical line:\n"<<LineUtils::getX(lvertic,5)<<endl;
    cout<<"getX vertical line:\n"<<LineUtils::getX(lvertic,10)<<endl;
    cout<<"getY vertical line:\n"<<LineUtils::getY(lvertic,0)<<endl;
    cout<<"getY vertical line:\n"<<LineUtils::getY(lvertic,5)<<endl;
    cout<<"getY vertical line:\n"<<LineUtils::getY(lvertic,10)<<endl;
    cout<<"getX horizontal line:\n"<<LineUtils::getX(lhoriz,0)<<endl;
    cout<<"getX horizontal line:\n"<<LineUtils::getX(lhoriz,5)<<endl;
    cout<<"getX horizontal line:\n"<<LineUtils::getX(lhoriz,10)<<endl;
    cout<<"getY horizontal line:\n"<<LineUtils::getY(lhoriz,0)<<endl;
    cout<<"getY horizontal line:\n"<<LineUtils::getY(lhoriz,5)<<endl;
    cout<<"getY horizontal line:\n"<<LineUtils::getY(lhoriz,10)<<endl;
    cout<<"getX normal line:\n"<<LineUtils::getX(lnormal,-2)<<endl;
    cout<<"getX normal line:\n"<<LineUtils::getX(lnormal,0)<<endl;
    cout<<"getX normal line:\n"<<LineUtils::getX(lnormal,2)<<endl;
    cout<<"getY normal line:\n"<<LineUtils::getY(lnormal,-2)<<endl;
    cout<<"getY normal line:\n"<<LineUtils::getY(lnormal,0)<<endl;
    cout<<"getY normal line:\n"<<LineUtils::getY(lnormal,2)<<endl;
    //test conversion to cartesian

    cout<<"Vertical line conversion within border:\n"<<LineUtils::getCartesian(lvertic,0,1,0,10)<<endl;
    cout<<"Vertical line lying on border line:\n"<<LineUtils::getCartesian(lvertic,0,10,0,10)<<endl;
    cout<<"Horizontal line lying on border line:\n"<<LineUtils::getCartesian(lhoriz,-10,1,0,10)<<endl;
    cout<<"Get y "<<LineUtils::getY(lhoriz,-10)<<endl;
    //cout<<"Vertical line conversion outside border:\n"<<LineUtils::getCartesian(p5,0,10,0,10)<<endl;

    cout<<"Normal line conversion within border:\n"<<LineUtils::getCartesian(lnormal,0,10,0,10)<<endl;
    cout<<"Normal line conversion outside border:\n"<<LineUtils::getCartesian(lnormal2,0,10,0,10)<<endl;

    cout<<"Test intersections\n"<<endl;
    cout<<"____________________________\n"<<endl;
    cout<<"Intersection with parallel lines"<<LineUtils::getIntersection(line_horiz,line_horiz2)<<endl;
    //test intersections with vectors
    std::vector<std::vector<cv::Point2f> > lines;
    std::vector<cv::Vec4f> L1;
    std::vector<cv::Vec4f> L2;
    L1.push_back(line_horiz);
    L1.push_back(line_horiz2);
    L2.push_back(line_horiz);
    L2.push_back(line_horiz2);
    L2.push_back(line_vertic);
    L2.push_back(line_vertic2);
    L2.push_back(line1);
    L2.push_back(line2);
    lines=LineUtils::getIntersection(L1,L2);
    cout<<"Intersection of vector of lines"<<endl;
    for (int i = 0; i < lines.size() ; ++i) {
        cout<<lines[i]<<endl;
    }
    //test The creation of the bounding box
    vector<Vec4f> v;
    v.push_back(line_horiz);
    v.push_back(line_horiz2);
    v.push_back(line_vertic);
    cout<<"Bounding box:"<<LineUtils::RectToVec(LineUtils::BoundingRect(v))<<endl;

    //test intersection with a ROI
    vector<Vec4f> intersec=LineUtils::getIntersectionWithRoi(L2,1,5,1,15);
    cout<<"Intersection with ROI"<<endl;
    for (int i = 0; i < intersec.size() ; ++i) {
        cout<<intersec[i]<<endl;
    }
    cout<<"Bounding box of intersection: \n"<<LineUtils::RectToVec(LineUtils::BoundingRect(intersec))<<endl;
    cout<<"\n\n"<<endl;
    cout<<"\n\n"<<endl;

    CV_Error(1,"END TEST");
}

void UnitTest::TestLineUtils2() {
    cout << "UnitTest LineUtils filter horiz lines:" << endl;
    cout << "_________________________\n" << endl;

    //create vector of lines
    vector<Vec4f> lines;
    lines.push_back(Vec4f(0, 10,
                          100, 10));
    lines.push_back(Vec4f(0, 0,
                          100, 0));
    lines.push_back(Vec4f(0, 30,
                          100, 30));
    lines.push_back(Vec4f(0, 10,
                          50, 10));

    lines.push_back(Vec4f(100, 10,
                          150, 10));
    lines.push_back(Vec4f(220, 10,
                          110, 10));
    lines.push_back(Vec4f(220, 30,
                          110, 30));
    lines.push_back(Vec4f(0, 30,
                          500, 30));
    lines.push_back(Vec4f(0, 27,
                          500, 35));
    cout << "Intersec:  " << LineUtils::getIntersection(lines[6], lines[2]) << endl;
    cout << "y1: " << LineUtils::getY(lines[2], 50) << " y2: " << LineUtils::getY(lines[6], 50) << endl;
    vector<vector<Vec4f> > lines_filtered = LineUtils::filterHorizDist(lines, 20, 5, 50, 2 * CV_PI / 180, 200);
    for (int i = 0; i < lines_filtered.size(); i++) {
        cout << lines_filtered[i][0] << ";" << lines_filtered[i][1] << endl;
    }
    cout << "\n\n" << endl;
    cout << "\n\n" << endl;

    CV_Error(1, "END TEST");
}

    void UnitTest::Test() {

        Mat cv_mat=Mat::zeros(4,4,CV_64FC1);
        //set last column
        cv_mat.at<double>(0,3)=1;
        cv_mat.at<double>(1,3)=2;
        cv_mat.at<double>(2,3)=3;
        cv_mat.at<double>(3,3)=1;
        //set idendity
        cv_mat.at<double>(0,0)=1;
        cv_mat.at<double>(1,1)=1;
        cv_mat.at<double>(2,2)=1;
        cout<<cv_mat;
        CV_Error(1, "END TEST");

    }

