#include <ros/ros.h>
#include <PalletDetection.h>
#include <string>
#include <opencv2/core/core.hpp>
#include "MatUtils.h"
#include <ros/package.h>
#include "PPoints.h"
#include "PPoint.h"
#include "PPoint3.h"
#include "PPoints3.h"
#include "PalletModel.h"
/*--------------------------------------------------------------------
 * main()
 * Main function to set up ROS node.
 *------------------------------------------------------------------*/

int main(int argc, char **argv)
{
    // Set up ROS.
    ros::init(argc, argv, "pallet_detection_node");
    ros::NodeHandle n;

    ROS_INFO("Starting pallet_detection_node..");
    // Initialize node parameters from launch file or command line.
    // Use a private node handle so that multiple instances of the node can be run simultaneously
    // while using different parameters.

    PalletDetection* pallet_detection= new PalletDetection(n);

    // Tell ROS how fast to run this node.
    ros::Rate r(40);

    //test transformation
    /*Mat t= Mat::eye(4,4,CV_32FC1);
    t.at<float>(0,3)=5;
    t.at<float>(1,3)=4;
    t.at<float>(2,3)=3;
    t.at<float>(3,0)=0;
    t.at<float>(3,1)=0;
    t.at<float>(3,2)=0;
    t.at<float>(3,3)=1;
    cout<<t<<endl;

    model.setTransformation3D(t);
    model.transformPointGroup();
    Mat temp;
    model.getAllPointsTransf(temp);
    cout<<"Points Transf dump \n"<<temp<<endl;
    model.getAllPoints(temp);
    cout<<"Points dump \n"<<temp<<endl;
    std::vector<Point3f> p;
    MatUtils::MatToPoint3f(temp,p);
    for(int i=0;i<p.size();i++){
        cout<<p[i].x<<"\t \t"<<p[i].y<<"\t \t"<<p[i].z<<endl;
    }
    cout<<Mat(p)<<endl;
    cout<<p<<endl;
    Mat mp=Mat(p);
    cout<<mp.rows<<"\t"<<mp.cols<<endl;
    transpose(mp,mp);
    cout<<mp.rows<<"\t"<<mp.cols<<endl;

    cout<<mp<<endl;
    vector< PPoint3<double> > points;
    PPoint3<double> ppoint(1.123456789,2.123456789,3.123456789);
    PPoint3<double> ppoint2(3.123456789,1.123456789,9.123456789);
    cout<<ppoint.toMat()<<endl;
    cout<<ppoint2.toMat()<<endl;
    points.push_back(ppoint);
    points.push_back(ppoint2);
    PPoints3<double> ppoints(points);

    cout<<ppoints.toMat()<<endl;*/

    while(n.ok())
    {
        pallet_detection->MainLoop();
        ros::spinOnce();
        r.sleep();
    }

    return 0;
}; // end main()